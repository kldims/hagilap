package com.hagilap.android.hagilap.match.presenter;

/**
 * Created by kdimla on 4/16/16.
 */
public interface MatchPresenter {
    void call();

    void sms();

    void resolve();
}
