package com.hagilap.android.hagilap.home.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.common.base.BaseFragment;
import com.hagilap.android.hagilap.data.service.event.RequestError;
import com.hagilap.android.hagilap.data.service.event.RequestSuccess;
import com.hagilap.android.hagilap.home.activity.HomeContainer;
import com.hagilap.android.hagilap.home.presenter.MapPresenter;
import com.hagilap.android.hagilap.home.presenter.impl.MapPresenterImpl;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by kdimla on 4/16/16.
 */
public class MapFragment extends BaseFragment implements com.hagilap.android.hagilap.home.view.MapView {
    private HomeContainer mBaseContainer;
    private MapPresenter mMapPresenter;
    private GoogleMap mMap;
    private Marker mCenterMarker;

    @Bind(R.id.hagilap_fragment_map_mapview)
    MapView mapView;

    public static MapFragment newInstance() {
        MapFragment mapFragment = new MapFragment();
        Bundle arguments = new Bundle();
        mapFragment.setArguments(arguments);
        return mapFragment;
    }

    @Override
    public void onReceiveSuccess(RequestSuccess requestSuccess) {

    }

    @Override
    public void onReceiveError(RequestError requestError) {

    }

    @Override
    public void plotCenter(double lng, double lat) {
        if (mCenterMarker != null) {
            mCenterMarker.remove();
        }
        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
        markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.me));
        if (mMap != null)
            mCenterMarker = mMap.addMarker(markerOption);
    }

    @Override
    public void plotUsers(double lng, double lat) {
        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
        markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.hagilaper));
        if (mMap != null) {
            mMap.addMarker(markerOption);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hagilap_fragment_map, container, false);
        ButterKnife.bind(this, view);
        initialize(savedInstanceState);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mBaseContainer = (HomeContainer) getActivity();
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
        if (mMapPresenter != null) {
            mMapPresenter.plotCurrentLocation();
            mMapPresenter.plotUsers();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @OnClick(R.id.hagilap_fragment_map_ask)
    public void onClick() {
        mBaseContainer.showAskForHelp();
    }

    private void initialize(Bundle savedInstanceState) {
        mMapPresenter = new MapPresenterImpl(mBaseContainer, this);
        initializeMap(savedInstanceState);
    }

    private void initializeMap(Bundle bundle) {
        mapView.onCreate(bundle);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.getUiSettings().setCompassEnabled(true);
                mMapPresenter.plotCurrentLocation();
                mMapPresenter.plotUsers();

            }
        });
    }

    @Override
    public void refreshCenter(double lng, double lat) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(lat,
                lng), 18);
        if (mMap != null) {
            mMap.animateCamera(cameraUpdate);
            plotCenter(lng, lat);
        }
    }

}
