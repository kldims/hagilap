package com.hagilap.android.hagilap.offerhelp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.common.base.BaseContainer;
import com.hagilap.android.hagilap.common.base.BaseFragment;
import com.hagilap.android.hagilap.common.constants.RequestCode;
import com.hagilap.android.hagilap.common.utils.EventbusUtils;
import com.hagilap.android.hagilap.data.gcm.model.InquiryPush;
import com.hagilap.android.hagilap.data.service.event.Request;
import com.hagilap.android.hagilap.data.service.event.RequestError;
import com.hagilap.android.hagilap.data.service.event.RequestSuccess;
import com.hagilap.android.hagilap.getoffers.adapter.OffersAdapter;
import com.hagilap.android.hagilap.getoffers.presenter.impl.OffersPresenterImpl;
import com.hagilap.android.hagilap.offerhelp.adapter.WhoNeedsAdapter;
import com.hagilap.android.hagilap.offerhelp.presenter.OfferHelpPresenter;
import com.hagilap.android.hagilap.offerhelp.presenter.impl.OfferHelpPresentrImpl;
import com.hagilap.android.hagilap.offerhelp.view.OfferHelpView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by kdimla on 4/17/16.
 */
public class OfferHelpFragment extends BaseFragment implements OfferHelpView {
    private BaseContainer mBaseContainer;
    private OfferHelpPresenter mPresenter;

    @Bind(R.id.hagilap_options_listview)
    ListView mListView;

    private WhoNeedsAdapter madapter;

    public static OfferHelpFragment newInstance() {
        OfferHelpFragment mOfferHelpFragment = new OfferHelpFragment();
        Bundle arguments = new Bundle();
        mOfferHelpFragment.setArguments(arguments);
        return mOfferHelpFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hagilap_fragment_ask_options, container, false);
        ButterKnife.bind(this, view);
        initialize();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mBaseContainer = (BaseContainer) getActivity();
    }

    @Override
    public void onReceiveSuccess(RequestSuccess requestSuccess) {
        switch (requestSuccess.getmRequestCode()) {
            case RequestCode.LOCAL_GET_INQUIRIES:
                if(requestSuccess.getmResponseObject() != null) {
                    if(madapter == null){
                        madapter = new WhoNeedsAdapter(mBaseContainer.getContext(), mPresenter.getOffers(), new WhoNeedsAdapter.OnHelpListener() {
                            @Override
                            public void onHelp(int position) {
                                mPresenter.onHelp(position);
                            }
                        });

                        mListView.setAdapter(madapter);
                    }
                    mPresenter.setInquiries((List<InquiryPush>) requestSuccess.getmResponseObject());
                }
                break;
            case RequestCode.GCM_MSG_RECEIVED:
                Timber.i("KL DIMS::: dkashdksajd123");
                addInquiry((InquiryPush) requestSuccess.getmResponseObject());
                refreshList();
                break;
        }
    }

    @Override
    public void onReceiveError(RequestError requestError) {

    }

    @Override
    public void setCount(int count) {

    }

    @Override
    public void addInquiry(InquiryPush inquiryPush) {
        mPresenter.addInquiries(inquiryPush);
    }

    @Override
    public void refreshList() {
        mBaseContainer.hideLoadingDialog();
        if(madapter != null) {
            madapter.notifyDataSetChanged();
        }else{
            madapter = new WhoNeedsAdapter(mBaseContainer.getContext(), mPresenter.getOffers(), new WhoNeedsAdapter.OnHelpListener() {
                @Override
                public void onHelp(int position) {
                    mPresenter.onHelp(position);
                }
            });
            mListView.setAdapter(madapter);
        }
    }

    private void initialize() {
        mPresenter = new OfferHelpPresentrImpl(mBaseContainer, this);
        EventbusUtils.postRequest(new Request(RequestCode.LOCAL_GET_INQUIRIES));
        madapter = new WhoNeedsAdapter(mBaseContainer.getContext(), mPresenter.getOffers(), new WhoNeedsAdapter.OnHelpListener() {
            @Override
            public void onHelp(int position) {
                    mPresenter.onHelp(position);
            }
        });
        mListView.setAdapter(madapter);
    }
}
