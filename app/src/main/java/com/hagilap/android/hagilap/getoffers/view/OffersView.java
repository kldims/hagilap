package com.hagilap.android.hagilap.getoffers.view;

import com.hagilap.android.hagilap.data.gcm.model.OfferPush;

/**
 * Created by kdimla on 4/16/16.
 */
public interface OffersView {
    void setOfferCount(int count);
    void addOfferToList(OfferPush offerPush);
    void refreshList();
}
