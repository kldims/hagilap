package com.hagilap.android.hagilap.match.presenter.impl;

import android.text.TextUtils;

import com.hagilap.android.hagilap.common.HagilapApplication;
import com.hagilap.android.hagilap.common.constants.RequestCode;
import com.hagilap.android.hagilap.common.utils.EventbusUtils;
import com.hagilap.android.hagilap.data.api.request.Params;
import com.hagilap.android.hagilap.data.gcm.model.AcceptOfferPush;
import com.hagilap.android.hagilap.data.model.AcceptedOffer;
import com.hagilap.android.hagilap.data.service.event.Request;
import com.hagilap.android.hagilap.match.activity.MatchContainer;
import com.hagilap.android.hagilap.match.presenter.MatchPresenter;
import com.hagilap.android.hagilap.match.view.MatchView;

/**
 * Created by kdimla on 4/16/16.
 */
public class MatchPresenterImpl implements MatchPresenter {
    private MatchContainer mBaseContainer;
    private MatchView mMatchView;
    private String inquiryId;
    private String offerId;
    private AcceptedOffer acceptedOffer;
    private AcceptOfferPush acceptOfferPush;

    public MatchPresenterImpl(MatchContainer baseContainer, MatchView matchView, String inquiryid) {
        this.mBaseContainer = baseContainer;
        this.mMatchView = matchView;
        this.inquiryId = inquiryid;
//        initialize();
    }

    @Override
    public void call() {
        mBaseContainer.call("");//acceptedOffer.getProfile().getNumber());
    }

    @Override
    public void sms() {
        mBaseContainer.sms("");//acceptedOffer.getProfile().getNumber());
    }

    @Override
    public void resolve() {
        Request request = new Request(RequestCode.API_ACCEPT_INQUIRY);
        request.setParameters(Params.getResolveInquiryParams(inquiryId));
        EventbusUtils.postRequest(request);
    }

    private void initialize() {
        if (acceptedOffer != null) {
            mMatchView.setHelperDescription(acceptedOffer.getDetails().getItem());
            mMatchView.setHelperName(acceptedOffer.getProfile().getName());
            mMatchView.setHelperDistance(acceptedOffer.getDistance() + "km away from you");
            mMatchView.plotOther(acceptedOffer.getProfile().getLocation().getLongitude(),
                    acceptedOffer.getProfile().getLocation().getLatitude());
            mMatchView.plotSelf(HagilapApplication.getmGoogleApiManager().getLastKnownLocation().getLongitude(),
                    HagilapApplication.getmGoogleApiManager().getLastKnownLocation().getLatitude());
        } else {
            mMatchView.setHelperDescription(acceptOfferPush.getDetails().getItem());
            mMatchView.setHelperName(acceptOfferPush.getProfile().getName());
            mMatchView.setHelperDistance("10 km away from you");
            mMatchView.plotOther(acceptOfferPush.getProfile().getLocation().getLongitude(),
                    acceptOfferPush.getProfile().getLocation().getLatitude());
            mMatchView.plotSelf(HagilapApplication.getmGoogleApiManager().getLastKnownLocation().getLongitude(),
                    HagilapApplication.getmGoogleApiManager().getLastKnownLocation().getLatitude());
        }
    }

}
