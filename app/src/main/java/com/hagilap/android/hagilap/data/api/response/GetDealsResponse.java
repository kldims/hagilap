package com.hagilap.android.hagilap.data.api.response;

import com.google.gson.annotations.SerializedName;
import com.hagilap.android.hagilap.data.model.Deal;

import java.util.ArrayList;

/**
 * Created by kdimla on 4/16/16.
 */
public class GetDealsResponse {
    @SerializedName("deals")
    private ArrayList<Deal> deals;
    @SerializedName("total")
    private String total;

    public ArrayList<Deal> getDeals() {
        return deals;
    }

    public void setDeals(ArrayList<Deal> deals) {
        this.deals = deals;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
