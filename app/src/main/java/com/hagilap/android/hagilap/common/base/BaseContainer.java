package com.hagilap.android.hagilap.common.base;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.hagilap.android.hagilap.data.service.event.Request;

/**
 * Created by kdimla on 4/16/16.
 */
public interface BaseContainer {
    Context getContext();

    Application getApp();

    Resources getAppResources();

    FragmentManager getBaseFragmentManager();

    void setToolBarTitle(String title);

    void showToolBar(boolean show);

    View getBaseView();

    void showLoadingDialog(String title, String message);

    void hideLoadingDialog();


    void postRequest(Request request);

    void startNewActivity(Intent intent, int requestCode);

    void showHome(boolean show);
}
