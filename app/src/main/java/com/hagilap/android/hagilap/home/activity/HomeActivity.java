package com.hagilap.android.hagilap.home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.askhelp.activity.AskActivity;
import com.hagilap.android.hagilap.common.base.BaseActivity;
import com.hagilap.android.hagilap.common.constants.RequestCode;
import com.hagilap.android.hagilap.common.utils.FragmentUtils;
import com.hagilap.android.hagilap.common.utils.SnackbarUtils;
import com.hagilap.android.hagilap.common.widget.AvenirTextView;
import com.hagilap.android.hagilap.data.gcm.model.InquiryPush;
import com.hagilap.android.hagilap.data.service.event.Request;
import com.hagilap.android.hagilap.data.service.event.RequestError;
import com.hagilap.android.hagilap.data.service.event.RequestSuccess;
import com.hagilap.android.hagilap.home.fragment.MapFragment;
import com.hagilap.android.hagilap.offerhelp.activity.OfferHelpActivity;

import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by kdimla on 4/16/16.
 */
public class HomeActivity extends BaseActivity implements HomeContainer, NavigationView.OnNavigationItemSelectedListener {

    @Bind(R.id.hagilap_home_drawer_layout)
    DrawerLayout mDrawerLayout;
    @Bind(R.id.hagilap_common_fragment_holder)
    FrameLayout mBaseFragmentLayout;
    TextView mHeaderViewText;
    @Bind(R.id.hagilap_home_coordinator)
    CoordinatorLayout mCoordinatorLayout;

    private NavigationView mNavigationView;
    private MapFragment mMapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hagilap_activity_home);
        ButterKnife.bind(this);
        initialize();
        initializeActionBar();
        setToolBarTitle("Hagilap");
        showHome(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void showToolBar(boolean show) {
        if (show) {
            mToolbar.setVisibility(View.VISIBLE);
        } else {
            mToolbar.setVisibility(View.GONE);
        }
    }

    @Override
    public View getBaseView() {
        return mBaseFragmentLayout;
    }

    @Override
    public void toggleDrawer() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
        } else {
            mDrawerLayout.openDrawer(mDrawerLayout);
        }
    }

    @Override
    public void showNeedClinic() {

    }

    @Override
    public void showNotGood() {

    }

    @Override
    public void showMedEquipment() {

    }

    @Override
    public void showAskMedRep() {

    }

    @Override
    public void showWhoNeedsHelp() {
        startActivity(new Intent(this, OfferHelpActivity.class));
    }

    @Override
    public void showAskForHelp() {
        startActivity(new Intent(this, AskActivity.class));
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.hagilap_menu_hospital:
                toggleDrawer();
                break;
            case R.id.hagilap_menu_equipment:
                toggleDrawer();
                break;
            case R.id.hagilap_menu_medrep:
                toggleDrawer();
                break;
            case R.id.hagilap_menu_not_good:
                toggleDrawer();
                break;
            case R.id.hagilap_menu_whoneeds:
                showWhoNeedsHelp();
                break;
        }
        return true;
    }


    @Subscribe
    public void onEventMainThread(Request request) {

    }

    @Subscribe
    public void onEventMainThread(RequestSuccess requestSuccess) {
        switch (requestSuccess.getmRequestCode()) {
            case RequestCode.GCM_MSG_RECEIVED:
                if (requestSuccess.getmResponseObject() instanceof InquiryPush) {
                    if (((InquiryPush) requestSuccess.getmResponseObject()).getType().equals("INQUIRY")) {
                        SnackbarUtils.showSnackbar(getBaseView(), "Someone needs your help! check list");
                    }
                }
                break;
        }
    }

    @Subscribe
    public void onEventMainThread(RequestError requestError) {

    }

    private void initialize() {
        initializeNavigationDrawer();
        mMapFragment = MapFragment.newInstance();
        FragmentUtils.replaceFragment(this, R.id.hagilap_common_fragment_holder, mMapFragment);
    }

    private void initializeNavigationDrawer() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                mToolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.croo_navigation_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        View header = mNavigationView.getHeaderView(0);
        mHeaderViewText = (AvenirTextView) header.findViewById(R.id.hagilap_header_view_text);
    }

}
