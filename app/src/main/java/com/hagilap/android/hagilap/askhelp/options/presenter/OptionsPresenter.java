package com.hagilap.android.hagilap.askhelp.options.presenter;

import com.hagilap.android.hagilap.data.model.Option;

import java.util.List;

/**
 * Created by kdimla on 4/16/16.
 */
public interface OptionsPresenter {
    List<Option> getOptions(int action);
    void doOnOptionClick(int position);
}
