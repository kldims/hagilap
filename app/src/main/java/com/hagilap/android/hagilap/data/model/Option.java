package com.hagilap.android.hagilap.data.model;

/**
 * Created by kdimla on 4/16/16.
 */
public class Option {
    private String optionName;

    public Option(String optionName) {
        this.optionName = optionName;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }
}
