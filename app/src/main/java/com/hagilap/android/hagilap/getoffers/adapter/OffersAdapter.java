package com.hagilap.android.hagilap.getoffers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.data.gcm.model.OfferPush;

import java.util.List;

/**
 * Created by kdimla on 4/16/16.
 */
public class OffersAdapter extends ArrayAdapter<OfferPush> {
    private List<OfferPush> mOffers;
    private Context context;
    private OnAcceptListener onAcceptListener;

    public interface OnAcceptListener {
        void onAccept(int position);
    }

    public OffersAdapter(Context context, List<OfferPush> objects, OnAcceptListener onAcceptListener) {
        super(context, R.layout.hagilap_offers_item, objects);
        this.mOffers = objects;
        this.context = context;
        this.onAcceptListener = onAcceptListener;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final OfferPush offerPush = mOffers.get(position);
        OffersViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.hagilap_offers_item, parent, false);
            holder = new OffersViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (OffersViewHolder) view.getTag();
        }

        holder.getmName().setText(offerPush.getProfile().getName());
        holder.getmDistance().setText(offerPush.getDistance() + "km away from you");
        holder.getmAccept().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAcceptListener.onAccept(position);
            }
        });

        return view;
    }
}
