package com.hagilap.android.hagilap.askhelp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.askhelp.gallery.fragment.GalleryFragment;
import com.hagilap.android.hagilap.askhelp.options.fragment.OptionsFragment;
import com.hagilap.android.hagilap.common.base.BaseActivity;
import com.hagilap.android.hagilap.common.constants.Action;
import com.hagilap.android.hagilap.common.constants.RequestCode;
import com.hagilap.android.hagilap.common.utils.FragmentUtils;
import com.hagilap.android.hagilap.data.api.response.CreateInquiryResponse;
import com.hagilap.android.hagilap.data.service.event.Request;
import com.hagilap.android.hagilap.data.service.event.RequestError;
import com.hagilap.android.hagilap.data.service.event.RequestSuccess;
import com.hagilap.android.hagilap.getoffers.fragment.OffersFragment;
import com.hagilap.android.hagilap.match.activity.MatchActivity;

import org.greenrobot.eventbus.Subscribe;

import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by kdimla on 4/16/16.
 */
public class AskActivity extends BaseActivity implements AskContainer {
    private GalleryFragment mGalleryFragment;
    private OptionsFragment mOptionsFragment;
    private OffersFragment mOffersFragment;
    private String inquiry;
    private int action;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hagilap_activity_ask);
        ButterKnife.bind(this);
        Timber.i("KL:: ON CREATE: " + System.currentTimeMillis());
        initialize();
        initializeActionBar();
        setToolBarTitle("What do you need?");
        showHome(false);
    }
    @Override
    public void showOptionsFragment(int action) {
        this.action = action;
        FragmentUtils.replaceFragment(this, R.id.hagilap_ask_holder, mOptionsFragment);
        mOptionsFragment.setAction(action);
    }

    @Override
    public void showGetOffersFragment() {
        FragmentUtils.replaceFragment(this, R.id.hagilap_ask_holder, mOffersFragment);
        mOffersFragment.setAction(action);
        mOffersFragment.setInquiry(inquiry);
    }

    @Override
    public View getBaseView() {
        return null;
    }


    @Subscribe
    public void onEventMainThread(Request request) {

    }

    @Subscribe
    public void onEventMainThread(RequestSuccess requestSuccess) {
        switch (requestSuccess.getmRequestCode()){
            case RequestCode.API_ACCEPT_OFFER:
                Timber.i("KL DIMS::: SUCC: "+requestSuccess.getmRequest().getParameters().get("inquiry"));
                Intent intent = new Intent(this, MatchActivity.class);
                intent.putExtra("inquiry",
                        (String)requestSuccess.getmRequest().getParameters().get("inquiry"));
                intent.putExtra("offer",
                        (String)requestSuccess.getmRequest().getParameters().get("offer"));
                intent.putExtra("accepted",
                        true);
                startActivity(intent);
                finish();
                break;
            case RequestCode.API_CREATE_INQUIRY:
                mOptionsFragment.onReceiveSuccess(requestSuccess);
                inquiry = ((CreateInquiryResponse)requestSuccess.getmResponseObject()).getInquiryid();
                Timber.i("KL DIMS::: INQUIRY: " + inquiry);
                hideLoadingDialog();
                break;
            case RequestCode.GCM_MSG_RECEIVED:
                mOffersFragment.onReceiveSuccess(requestSuccess);
                break;

        }
    }

    @Subscribe
    public void onEventMainThread(RequestError requestError) {

    }

    @Override
    public Activity getACtivity() {
        return this;
    }

    @Override
    public String retrieveInquiryId() {
        return inquiry;
    }

    private void initialize(){
        mGalleryFragment = GalleryFragment.newInstance();
        mOptionsFragment = OptionsFragment.newInstance();
        mOffersFragment = OffersFragment.newInstance();

        FragmentUtils.replaceFragment(this, R.id.hagilap_ask_holder, mGalleryFragment);
    }
}
