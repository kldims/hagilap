package com.hagilap.android.hagilap.data.api.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kdimla on 4/16/16.
 */
public class OfferResponse extends Default {
    @SerializedName("offerId")
    private String offerId;
    @SerializedName("status")
    private String status;

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
