package com.hagilap.android.hagilap.data.gcm.model;

import com.google.gson.annotations.SerializedName;
import com.hagilap.android.hagilap.data.model.Inquiry;
import com.hagilap.android.hagilap.data.model.Profile;

/**
 * Created by kdimla on 4/16/16.
 */
public class InquiryPush {
    @SerializedName("type")
    private String type;
    @SerializedName("details")
    private Inquiry details;
    @SerializedName("profile")
    private Profile profile;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Inquiry getDetails() {
        return details;
    }

    public void setDetails(Inquiry details) {
        this.details = details;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }
}
