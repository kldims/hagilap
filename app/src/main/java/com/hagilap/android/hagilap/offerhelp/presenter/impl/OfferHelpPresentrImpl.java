package com.hagilap.android.hagilap.offerhelp.presenter.impl;

import com.hagilap.android.hagilap.common.HagilapApplication;
import com.hagilap.android.hagilap.common.base.BaseContainer;
import com.hagilap.android.hagilap.common.constants.RequestCode;
import com.hagilap.android.hagilap.common.utils.EventbusUtils;
import com.hagilap.android.hagilap.data.api.request.Body;
import com.hagilap.android.hagilap.data.api.request.Params;
import com.hagilap.android.hagilap.data.gcm.model.InquiryPush;
import com.hagilap.android.hagilap.data.model.Location;
import com.hagilap.android.hagilap.data.model.Offer;
import com.hagilap.android.hagilap.data.service.event.Request;
import com.hagilap.android.hagilap.offerhelp.activity.OfferHelpContainer;
import com.hagilap.android.hagilap.offerhelp.presenter.OfferHelpPresenter;
import com.hagilap.android.hagilap.offerhelp.view.OfferHelpView;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by kdimla on 4/17/16.
 */
public class OfferHelpPresentrImpl implements OfferHelpPresenter {
    private List<InquiryPush> mOffers = new ArrayList<>();
    private BaseContainer mBaseContainer;
    private OfferHelpView mOffersView;
    private String inquiryId;

    public OfferHelpPresentrImpl(BaseContainer baseContainer, OfferHelpView offersView) {
        this.mBaseContainer = baseContainer;
        this.mOffersView = offersView;
    }

    @Override
    public List<InquiryPush> getOffers() {
        return mOffers;
    }

    @Override
    public void onHelp(int position) {
        ((OfferHelpContainer)mBaseContainer).setInquiryId(mOffers.get(position).getDetails().getId());
        Request request = new Request(RequestCode.API_CREATE_OFFER);
        request.setParameters(Params.getCreateOfferParams(mOffers.get(position).getDetails().getId()));
        Offer offer = new Offer();
        Location location = new Location();
        location.setLatitude(HagilapApplication.getmGoogleApiManager().getLastKnownLocation().getLatitude());
        location.setLongitude(HagilapApplication.getmGoogleApiManager().getLastKnownLocation().getLongitude());
        offer.setLocation(location);
        request.setBody(Body.getCreateOffer(offer));
        EventbusUtils.postRequest(request);
        mBaseContainer.showLoadingDialog("Offering help","please wait...");
    }

    @Override
    public void setInquiries(List<InquiryPush> inquiries) {
        this.mOffers = inquiries;
        mOffersView.refreshList();
    }

    @Override
    public void addInquiries(InquiryPush inquiry) {
        this.mOffers.add(inquiry);
        Timber.i("KL DIMS::: size"+this.mOffers.size());
        mOffersView.refreshList();
    }
}
