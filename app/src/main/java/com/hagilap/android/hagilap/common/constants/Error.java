package com.hagilap.android.hagilap.common.constants;

/**
 * Created by kdimla on 4/16/16.
 */
public interface Error {
    int GCM_ERROR = -1;
    int API_ERROR = -2;
}
