package com.hagilap.android.hagilap.askhelp.options.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.data.model.Option;

import java.util.List;

/**
 * Created by kdimla on 4/16/16.
 */
public class OptionsAdapter extends ArrayAdapter<Option> {
    private List<Option> mOptions;
    private Context context;

    public OptionsAdapter(Context context, List<Option> objects) {
        super(context, R.layout.hagilap_option_item, objects);
        this.mOptions = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final Option option = mOptions.get(position);
        OptionViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.hagilap_option_item, parent, false);
            holder = new OptionViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (OptionViewHolder) view.getTag();
        }

        holder.getOptionName().setText(option.getOptionName());

        return view;
    }
}
