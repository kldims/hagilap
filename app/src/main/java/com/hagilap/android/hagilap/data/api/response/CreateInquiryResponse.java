package com.hagilap.android.hagilap.data.api.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kdimla on 4/16/16.
 */
public class CreateInquiryResponse extends Default {
    @SerializedName("inquiryId")
    private String inquiryid;

    public String getInquiryid() {
        return inquiryid;
    }

    public void setInquiryid(String inquiryid) {
        this.inquiryid = inquiryid;
    }
}
