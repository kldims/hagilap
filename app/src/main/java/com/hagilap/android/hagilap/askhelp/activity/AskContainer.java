package com.hagilap.android.hagilap.askhelp.activity;

import android.app.Activity;

import com.hagilap.android.hagilap.common.base.BaseContainer;

/**
 * Created by kdimla on 4/16/16.
 */
public interface AskContainer extends BaseContainer{
    void showOptionsFragment(int action);
    void showGetOffersFragment();
    Activity getACtivity();
    String retrieveInquiryId();
}
