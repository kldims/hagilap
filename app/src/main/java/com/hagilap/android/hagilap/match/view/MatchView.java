package com.hagilap.android.hagilap.match.view;

/**
 * Created by kdimla on 4/16/16.
 */
public interface MatchView {
    void setHelperName(String name);
    void setHelperDescription(String description);
    void setHelperDistance(String distance);
    void plotSelf(double lng, double lat);
    void plotOther(double lng, double lat);
}
