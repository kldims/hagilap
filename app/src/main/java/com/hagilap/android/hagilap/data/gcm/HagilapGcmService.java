package com.hagilap.android.hagilap.data.gcm;

import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;
import com.hagilap.android.hagilap.common.constants.RequestCode;
import com.hagilap.android.hagilap.common.utils.EventbusUtils;
import com.hagilap.android.hagilap.data.service.event.Request;

import timber.log.Timber;

/**
 * Created by kdimla on 4/16/16.
 */
public class HagilapGcmService extends GcmListenerService {

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        Timber.i("KL DIMS::: From: " + from);
        Timber.i("KL DIMS::: Message: " + message);
        Request<String> request = new Request<>(RequestCode.GCM_MSG_RECEIVED);
        request.setmObject(message);
        EventbusUtils.postRequest(request);
    }
}
