package com.hagilap.android.hagilap.data.api.request;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kdimla on 4/16/16.
 */
public class Params {
    public static Map<String, String> getAcceptOfferParams(String inquiry, String offer){
        Map<String, String> params = new HashMap<>();
        params.put("inquiry",inquiry);
        params.put("offer", offer);
        return params;
    }

    public static Map<String, String> getResolveInquiryParams(String inquiry){
        Map<String, String> params = new HashMap<>();
        params.put("inquiry",inquiry);
        return params;
    }
    public static Map<String, String> getCreateOfferParams(String inquiry){
        Map<String, String> params = new HashMap<>();
        params.put("inquiry",inquiry);
        return params;
    }
}
