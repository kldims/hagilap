package com.hagilap.android.hagilap.offerhelp.view;

import com.hagilap.android.hagilap.data.gcm.model.InquiryPush;

/**
 * Created by kdimla on 4/17/16.
 */
public interface OfferHelpView {
    void setCount(int count);

    void addInquiry(InquiryPush inquiryPush);

    void refreshList();
}
