package com.hagilap.android.hagilap.askhelp.options.presenter.impl;

import com.hagilap.android.hagilap.askhelp.options.presenter.OptionsPresenter;
import com.hagilap.android.hagilap.askhelp.options.view.OptionsView;
import com.hagilap.android.hagilap.common.HagilapApplication;
import com.hagilap.android.hagilap.common.base.BaseContainer;
import com.hagilap.android.hagilap.common.constants.Action;
import com.hagilap.android.hagilap.common.constants.Category;
import com.hagilap.android.hagilap.common.constants.RequestCode;
import com.hagilap.android.hagilap.common.utils.EventbusUtils;
import com.hagilap.android.hagilap.data.api.HagilapApi;
import com.hagilap.android.hagilap.data.api.request.Body;
import com.hagilap.android.hagilap.data.model.Inquiry;
import com.hagilap.android.hagilap.data.model.Location;
import com.hagilap.android.hagilap.data.model.Option;
import com.hagilap.android.hagilap.data.service.event.Request;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by kdimla on 4/16/16.
 */
public class OptionsPresenterImpl implements OptionsPresenter{
    private BaseContainer mBaseContainer;
    private OptionsView mOptionsView;
    private String category;

    private List<Option> mOptions;

    public OptionsPresenterImpl(BaseContainer baseContainer, OptionsView optionsView) {
        this.mBaseContainer = baseContainer;
        this.mOptionsView = optionsView;
    }

    @Override
    public List<Option> getOptions(int action) {
        mOptions = new ArrayList<>();
        switch (action){
            case Action.HOSPITAL:
                category= Category.HOSPITAL;
                mockHospital();
                break;
            case Action.EQUIPMENT:
                category = Category.EQUIPMENT;
                mockEquipment();
                break;
            case Action.MEDREP:
                category = Category.MEDREP;
                mockMedrep();
                break;
            case Action.NOT_GOOD:
                category = Category.NOT_GOOD;
                mockNotGood();
                break;
        }
        return mOptions;
    }

    @Override
    public void doOnOptionClick(int position) {
        mBaseContainer.showLoadingDialog("Making an inquiry","Please wait...");
        Timber.i("ITEM CLICK: "+mOptions.get(position).getOptionName());
        Request request = new Request(RequestCode.API_CREATE_INQUIRY);
        android.location.Location loc = HagilapApplication.getmGoogleApiManager().getLastKnownLocation();

        Location location = new Location();
        location.setLongitude(loc.getLongitude());
        location.setLatitude(loc.getLatitude());

        Inquiry inquiry = new Inquiry();
        inquiry.setCategory(category);
        inquiry.setItem(mOptions.get(position).getOptionName());
        inquiry.setLocation(location);

        request.setBody(Body.getCreateInquiry(inquiry));

        EventbusUtils.postRequest(request);
    }

    private void mockHospital(){
        mOptions.add(new Option("Im having a heart attack."));
        mOptions.add(new Option("Im have a bullet wound."));
        mOptions.add(new Option("Im need a midwife."));
    }
    private void mockEquipment(){
        mOptions.add(new Option("Im having a heart attack."));
        mOptions.add(new Option("Im have a bullet wound."));
        mOptions.add(new Option("Im need a midwife."));
    }
    private void mockMedrep(){
        mOptions.add(new Option("Im having a heart attack."));
        mOptions.add(new Option("Im have a bullet wound."));
        mOptions.add(new Option("Im need a midwife."));
    }
    private void mockNotGood(){
        mOptions.add(new Option("Im having a heart attack."));
        mOptions.add(new Option("Im have a bullet wound."));
        mOptions.add(new Option("Im need a midwife."));
    }
}
