package com.hagilap.android.hagilap.common.constants;

/**
 * Created by kdimla on 4/16/16.
 */
public interface Endpoints {
    String REGISTER = "/users";
    String VERIFY = "/users/verify";
    String PROFILE = "/users/me";
    String MERCHANTS = "/merchants";
    String DEALS = "/merchants/{merchant}/deals";
    String INQUIRY = "/inquiries";
    String INQUIRY_DELETE = "/inquiries/{inquiry}";
    String OFFER = "/inquiries/{inquiry}/offers";
    String OFFER_DELETE = "/inquiries/{inquiry}/offers/{offer}";
}
