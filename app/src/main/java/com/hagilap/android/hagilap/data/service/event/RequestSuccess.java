package com.hagilap.android.hagilap.data.service.event;

/**
 * Created by kdimla on 4/16/16.
 */
public class RequestSuccess<T> {
    private T mResponseObject;
    private int mRequestCode;
    private Request mRequest;

    public RequestSuccess(Request request) {
        this.mRequest = request;
        this.mRequestCode = request.getRequestCode();
    }

    public void setmResponseObject(T mResponseObject) {
        this.mResponseObject = mResponseObject;
    }

    public T getmResponseObject() {
        return mResponseObject;
    }

    public int getmRequestCode() {
        return mRequestCode;
    }

    public Request getmRequest() {
        return mRequest;
    }
}
