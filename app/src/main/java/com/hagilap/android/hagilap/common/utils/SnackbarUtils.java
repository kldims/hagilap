package com.hagilap.android.hagilap.common.utils;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by kdimla on 4/16/16.
 */
public class SnackbarUtils {
    public static void showSnackbar(View view, CharSequence text, int duration) {
        Snackbar.make(view, text, duration).show();
    }

    public static void showSnackbar(View view, int textId, int duration) {
        Snackbar.make(view, textId, duration).show();
    }

    public static void showSnackbar(View view, int textId, String actionText, View.OnClickListener actionListener, int duration) {
        Snackbar.make(view, textId, duration)
                .setAction(actionText, actionListener)
                .show();
    }

    public static void showSnackbar(View view, CharSequence text) {
        showSnackbar(view, text, Snackbar.LENGTH_SHORT);
    }

    public static void showSnackbar(View view, int textId) {
        showSnackbar(view, textId, Snackbar.LENGTH_SHORT);
    }

    public static void showSnackbar(View view, int textId, String actionText, View.OnClickListener actionListener) {
        showSnackbar(view, textId, actionText, actionListener, Snackbar.LENGTH_SHORT);
    }
}
