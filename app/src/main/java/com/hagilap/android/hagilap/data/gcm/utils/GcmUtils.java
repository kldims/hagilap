package com.hagilap.android.hagilap.data.gcm.utils;

import android.content.Context;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.gson.Gson;
import com.hagilap.android.hagilap.data.gcm.model.AcceptOfferPush;
import com.hagilap.android.hagilap.data.gcm.model.InquiryPush;
import com.hagilap.android.hagilap.data.gcm.model.OfferPush;
import com.hagilap.android.hagilap.data.gcm.model.RejectOfferPush;

import timber.log.Timber;

/**
 * Created by kdimla on 4/16/16.
 */
public class GcmUtils {

    private static final String GCM_API_KEY = "AIzaSyDIJ-nSsSLjPsc8gvVKhhchZEahC8holHc";
    private static final String GCM_SENDER_ID = "230735104698";

    public static String handleGcmIntent(Context context) {
        try {
            InstanceID instanceID = InstanceID.getInstance(context);
            String token = instanceID.getToken(GCM_SENDER_ID,
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            Timber.i("KL DIMS::: GCM Registration Token: " + token);
            return token;
        } catch (Exception e) {
            Timber.i("KL DIMS::: Failed to complete token refresh", e);
            return null;
        }
    }

    public static Object parseMessage(String message) {
        Gson gson = new Gson();
        if(message != null) {
            if (message.contains("type\":\"INQUIRY\"")) {
                Timber.i("KL DIMS::: 1");
                InquiryPush inquiryPush = gson.fromJson(message, InquiryPush.class);
                return inquiryPush;
            } else if (message.contains("\"type\":\"OFFER\"")) {
                if (message.contains("\"status\":\"AWAITING\"")) {
                    Timber.i("KL DIMS::: 2");
                    OfferPush offerPush = gson.fromJson(message, OfferPush.class);
                    return offerPush;
                } else if (message.contains("\"status\",\"ACCEPTED\"")) {
                    Timber.i("KL DIMS::: 3");
                    AcceptOfferPush acceptOfferPush = gson.fromJson(message, AcceptOfferPush.class);
                    return acceptOfferPush;
                } else {
                    Timber.i("KL DIMS::: 4");
                    RejectOfferPush rejectOfferPush = gson.fromJson(message, RejectOfferPush.class);
                    return rejectOfferPush;
                }
            }
        }
        return null;
    }


}
