package com.hagilap.android.hagilap.getoffers.presenter.impl;

import android.location.Location;

import com.hagilap.android.hagilap.askhelp.activity.AskContainer;
import com.hagilap.android.hagilap.askhelp.options.view.OptionsView;
import com.hagilap.android.hagilap.common.HagilapApplication;
import com.hagilap.android.hagilap.common.base.BaseContainer;
import com.hagilap.android.hagilap.common.constants.RequestCode;
import com.hagilap.android.hagilap.common.utils.EventbusUtils;
import com.hagilap.android.hagilap.common.utils.SharedPreferenceUtils;
import com.hagilap.android.hagilap.data.api.request.Body;
import com.hagilap.android.hagilap.data.api.request.Params;
import com.hagilap.android.hagilap.data.gcm.model.OfferPush;
import com.hagilap.android.hagilap.data.service.event.Request;
import com.hagilap.android.hagilap.getoffers.presenter.OffersPresenter;
import com.hagilap.android.hagilap.getoffers.view.OffersView;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by kdimla on 4/16/16.
 */
public class OffersPresenterImpl implements OffersPresenter {
    private List<OfferPush> mOffers = new ArrayList<>();
    private BaseContainer mBaseContainer;
    private OffersView mOffersView;
    private String inquiryId;

    public OffersPresenterImpl(BaseContainer baseContainer, OffersView offersView) {
        this.mBaseContainer = baseContainer;
        this.mOffersView = offersView;
    }

    @Override
    public List<OfferPush> getOffers() {
        return mOffers;
    }

    @Override
    public void refreshOffers() {
        mOffersView.setOfferCount(mOffers.size());
        mOffersView.refreshList();
    }

    @Override
    public void acceptOffer(int position) {
        mBaseContainer.showLoadingDialog("Accept", "Accepting the offered help...");
        Timber.i("KL DIMS::: "+inquiryId);
        if(inquiryId == null){
            inquiryId = SharedPreferenceUtils.getHagilapName();
        }
        Timber.i("KL DIMS::: too "+inquiryId);
        Location location = HagilapApplication.getmGoogleApiManager().getLastKnownLocation();
        com.hagilap.android.hagilap.data.model.Location hagilapLocation = new com.hagilap.android.hagilap.data.model.Location();
        hagilapLocation.setLatitude(location.getLatitude());
        hagilapLocation.setLongitude(location.getLongitude());
        Request request = new Request(RequestCode.API_ACCEPT_OFFER);
        request.setParameters(Params.getAcceptOfferParams(inquiryId,
                mOffers.get(position).getOfferid()));
        request.setBody(Body.getAcceptOffer(hagilapLocation));
        EventbusUtils.postRequest(request);
    }

    @Override
    public void addNewOffer(OfferPush offerPush) {
        mOffers.add(offerPush);
        Timber.i("KL DIMS::: SLEEP 5   "+mOffers.size());
        refreshOffers();
    }

    @Override
    public void setInquiry(String inquiry) {
        this.inquiryId = inquiry;
    }
}
