package com.hagilap.android.hagilap.home.presenter;

/**
 * Created by kdimla on 4/16/16.
 */
public interface MapPresenter {
    void plotUsers();
    void plotCurrentLocation();
}
