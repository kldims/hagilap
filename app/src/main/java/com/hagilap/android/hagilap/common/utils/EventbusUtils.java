package com.hagilap.android.hagilap.common.utils;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by kdimla on 4/16/16.
 */
public class EventbusUtils {
    public static void register(Object object) {
        EventBus.getDefault().register(object);
    }

    public static void unregister(Object object) {
        EventBus.getDefault().unregister(object);
    }

    public static void postRequest(Object request) {
        EventBus.getDefault().post(request);
    }
}
