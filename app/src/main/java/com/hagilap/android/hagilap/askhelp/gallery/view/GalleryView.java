package com.hagilap.android.hagilap.askhelp.gallery.view;

/**
 * Created by kdimla on 4/16/16.
 */
public interface GalleryView {
    void showOptions(int action);
}
