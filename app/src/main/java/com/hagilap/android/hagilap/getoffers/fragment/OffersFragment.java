package com.hagilap.android.hagilap.getoffers.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.askhelp.activity.AskContainer;
import com.hagilap.android.hagilap.common.base.BaseFragment;
import com.hagilap.android.hagilap.common.constants.Action;
import com.hagilap.android.hagilap.common.constants.RequestCode;
import com.hagilap.android.hagilap.data.gcm.model.OfferPush;
import com.hagilap.android.hagilap.data.service.event.RequestError;
import com.hagilap.android.hagilap.data.service.event.RequestSuccess;
import com.hagilap.android.hagilap.getoffers.adapter.OffersAdapter;
import com.hagilap.android.hagilap.getoffers.presenter.OffersPresenter;
import com.hagilap.android.hagilap.getoffers.presenter.impl.OffersPresenterImpl;
import com.hagilap.android.hagilap.getoffers.view.OffersView;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by kdimla on 4/16/16.
 */
public class OffersFragment extends BaseFragment implements OffersView {
    private AskContainer mBaseContainer;
    private OffersPresenter mOfferPresenter;

    @Bind(R.id.hagilap_offers_list)
    ListView mOffersListView;
    @Bind(R.id.hagilap_offers_count)
    TextView mCountTextView;
    private OffersAdapter mOffersAdapter;
    private int action;
    private String inquiry;

    public static OffersFragment newInstance() {
        OffersFragment mapFragment = new OffersFragment();
        Bundle arguments = new Bundle();
        mapFragment.setArguments(arguments);
        return mapFragment;
    }

    @Override
    public void onReceiveSuccess(RequestSuccess requestSuccess) {
        Timber.i("KL DIMS::: SLEEP 1");
        switch (requestSuccess.getmRequestCode()) {
            case RequestCode.GCM_MSG_RECEIVED:
                if (requestSuccess.getmResponseObject() != null) {
                    Timber.i("KL DIMS::: SLEEP 2");
//                    if (requestSuccess.getmResponseObject() instanceof OfferPush) {
                    mBaseContainer.hideLoadingDialog();
                    if (mOfferPresenter == null) {
                        Timber.i("KL DIMS::: SLEEP 3");
                        initialize();
                    }
                    mOfferPresenter.addNewOffer((OfferPush) requestSuccess.getmResponseObject());
//                    }
                }
                break;
        }
    }

    @Override
    public void onReceiveError(RequestError requestError) {

    }

    @Override
    public void setOfferCount(final int count) {
//        mCountTextView.setText(count + getCountText());
        mBaseContainer.getACtivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCountTextView.setText(count + getCountText());
                mOfferPresenter.setInquiry(inquiry);
            }
        });
    }

    @Override
    public void addOfferToList(OfferPush offerPush) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hagilap_fragment_offers, container, false);
        ButterKnife.bind(this, view);
        initialize();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mBaseContainer = (AskContainer) getActivity();
    }

    @Override
    public void refreshList() {
        mBaseContainer.getACtivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mOffersAdapter.notifyDataSetChanged();
            }
        });
    }

    public void setAction(int action) {
        this.action = action;
    }
    public void setInquiry(String inquiry) {
        this.inquiry = inquiry;
//        if(mOfferPresenter != null) {
//            mOfferPresenter.setInquiry(inquiry);
//        }
    }

    private String getCountText() {
        switch (action) {
            case Action.HOSPITAL:
                return " hospitals can admit you";
            case Action.EQUIPMENT:
                return " people offered a help";
            case Action.MEDREP:
                return " people offered a help";
            case Action.NOT_GOOD:
                return " people offered a help";
        }
        return "";
    }

    private void initialize() {
        mOfferPresenter = new OffersPresenterImpl(mBaseContainer, this);
        mOffersAdapter = new OffersAdapter(mBaseContainer.getContext(), mOfferPresenter.getOffers(), new OffersAdapter.OnAcceptListener() {
            @Override
            public void onAccept(int position) {
                mOfferPresenter.acceptOffer(position);
            }
        });
        mOffersListView.setAdapter(mOffersAdapter);
    }
}
