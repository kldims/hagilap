package com.hagilap.android.hagilap.data.service.event;

import com.hagilap.android.hagilap.data.api.request.Body;

import java.util.Map;

import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedOutput;

/**
 * Created by kdimla on 4/16/16.
 */
public class Request<T> {
    private int mRequestCode;
    private String mPath;
    private byte[] mBody;
    private Map<String, String> mParameters;
    private Map<String, String> mHeaders;
    private T mObject;

    public Request(int requestCode, String path, byte[] body, Map<String, String> parameters) {
        this.mRequestCode = requestCode;
        this.mPath = path;
        this.mBody = body;
        this.mParameters = parameters;
    }

    public Request(int requestCode, String path, byte[] body) {
        this.mRequestCode = requestCode;
        this.mPath = path;
        this.mBody = body;
    }

    public Request(int requestCode, String path) {
        this.mRequestCode = requestCode;
        this.mPath = path;
    }

    public Request(int requestCode) {
        this.mRequestCode = requestCode;
    }

    public int getRequestCode() {
        return mRequestCode;
    }

    public void setRequestCode(int requestCode) {
        this.mRequestCode = requestCode;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        this.mPath = path;
    }

    public byte[] getBody() {
        return mBody;
    }

    public void setBody(byte[] body) {
        this.mBody = body;
    }

    public Map<String, String> getParameters() {
        return mParameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.mParameters = parameters;
    }

    public Map<String, String> getHeaders() {
        return mHeaders;
    }

    public void setHeaders(Map<String, String> headers) {
        this.mHeaders = mHeaders;
    }

    public TypedOutput getBodyAsTypedOutput() {
        return new TypedByteArray(Body.ContentType.JSON, mBody);
    }

    public TypedOutput getBodyAsTypedOutput(String contentType) {
        return new TypedByteArray(contentType, mBody);
    }

    public T getmObject() {
        return mObject;
    }

    public void setmObject(T mObject) {
        this.mObject = mObject;
    }
}
