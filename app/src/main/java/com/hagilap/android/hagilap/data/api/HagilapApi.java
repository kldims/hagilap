package com.hagilap.android.hagilap.data.api;

import com.hagilap.android.hagilap.common.constants.Endpoints;
import com.hagilap.android.hagilap.data.api.response.CreateInquiryResponse;
import com.hagilap.android.hagilap.data.api.response.Default;
import com.hagilap.android.hagilap.data.api.response.OfferResponse;
import com.hagilap.android.hagilap.data.api.response.RegisterResponse;
import com.hagilap.android.hagilap.data.model.AcceptedOffer;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.mime.TypedOutput;

/**
 * Created by kdimla on 4/16/16.
 */
public interface HagilapApi {
    @POST(Endpoints.REGISTER)
    void register(@Body TypedOutput body, Callback<RegisterResponse> callback);

    @POST(Endpoints.VERIFY)
    void verify(@Body TypedOutput body, Callback<RegisterResponse> callback);

    @GET(Endpoints.PROFILE)
    void getProfile(@Body TypedOutput body, Callback<RegisterResponse> callback);

    @PUT(Endpoints.PROFILE)
    void updateProfile(@Body TypedOutput body, Callback<RegisterResponse> callback);

    @GET(Endpoints.MERCHANTS)
    void getMerchants(@Body TypedOutput body, Callback<RegisterResponse> callback);

    @GET(Endpoints.DEALS)
    void getDealsFromMerchant(@Path("merchant") String merchant, @Body TypedOutput body, Callback<RegisterResponse> callback);

    @POST(Endpoints.INQUIRY)
    void poll(@Body TypedOutput body, Callback<CreateInquiryResponse> callback);

    @DELETE(Endpoints.INQUIRY_DELETE)
    void resolvePoll(@Path("inquiry") String inquiry, Callback<Default> callback);

    @POST(Endpoints.OFFER)
    void offer(@Path("inquiry") String inquiry, @Body TypedOutput body, Callback<OfferResponse> callback);

    @DELETE(Endpoints.OFFER_DELETE)
    void acceptOffer(@Path("inquiry") String inquiry, @Path("offer") String offer,
                      Callback<AcceptedOffer> callback);
}
