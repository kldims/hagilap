package com.hagilap.android.hagilap.offerhelp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.data.gcm.model.InquiryPush;

import java.util.List;

/**
 * Created by kdimla on 4/17/16.
 */
public class WhoNeedsAdapter extends ArrayAdapter<InquiryPush> {
    private List<InquiryPush> mInquiries;
    private Context context;
    private OnHelpListener onAcceptListener;

    public interface OnHelpListener {
        void onHelp(int position);
    }

    public WhoNeedsAdapter(Context context, List<InquiryPush> objects, OnHelpListener onAcceptListener) {
        super(context, R.layout.hagilap_whoneeds_item, objects);
        this.mInquiries = objects;
        this.context = context;
        this.onAcceptListener = onAcceptListener;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final InquiryPush inquiryPush = mInquiries.get(position);
        WhoNeedsViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.hagilap_whoneeds_item, parent, false);
            holder = new WhoNeedsViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (WhoNeedsViewHolder) view.getTag();
        }

        holder.getmName().setText(inquiryPush.getProfile().getName());
        holder.getmDistance().setText("8 km away from you");
        holder.getmHelp().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAcceptListener.onHelp(position);
            }
        });

        return view;
    }
}
