package com.hagilap.android.hagilap.common.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kdimla on 4/16/16.
 */
public class PhoneNumberUtils {
    private static final String PH_REGEX = "^([+]{0,1}63[89]|[89]|0[89]){1}\\d{9}$";

    public static int getCountryCode(String msisdn, String iso) {
        PhoneNumberUtil util = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber pn = util.parse(msisdn, iso);
            return pn.getCountryCode();
        } catch (NumberParseException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static Boolean isValidNumber(String msisdn, String iso) {
        PhoneNumberUtil util = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber pn = util.parse(msisdn, iso);
            return true;
        } catch (NumberParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getNumber(Context context) {
        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return tMgr.getLine1Number();
    }

    public static boolean validatePHNumber(String msisdn) {
        Pattern pattern = Pattern.compile(PH_REGEX);
        Matcher matcher = pattern.matcher(msisdn);
        if (matcher.find()) {
            return true;
        }
        return false;
    }

    public static String sanitizeNumber(String msisdn) {
        if (validatePHNumber(msisdn)) {
            msisdn = msisdn.replaceAll("^+", "");
            msisdn = msisdn.replaceAll("^0(\\d{9})", "63$1");
            msisdn = msisdn.replaceAll("^9(\\d{9})", "639$1");
        }
        return msisdn;
    }

    public static boolean isGlobalNumber(String msisdn){
        return android.telephony.PhoneNumberUtils.isGlobalPhoneNumber(msisdn);
    }
}
