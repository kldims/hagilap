package com.hagilap.android.hagilap.common.utils;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/**
 * Created by kdimla on 4/16/16.
 */
public class GoogleApiManager {
    private static GoogleApiManager sInstance;
    private GoogleApiClient mGoogleApiClient;
    private GoogleApiClient.ConnectionCallbacks callbacks = new GoogleApiClient.ConnectionCallbacks() {
        @Override
        public void onConnected(Bundle bundle) {
            Location location = getLastKnownLocation();

        }

        @Override
        public void onConnectionSuspended(int i) {

        }
    };

    private GoogleApiClient.OnConnectionFailedListener connectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {

        }
    };

    public static GoogleApiManager getInstance(Context context){
        if(sInstance == null){
            sInstance = new GoogleApiManager(context);
        }
        return sInstance;
    }

    public GoogleApiManager(Context context){
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(callbacks)
                    .addOnConnectionFailedListener(connectionFailedListener)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    public void connect(){
        mGoogleApiClient.connect();
    }

    public void disconnect(){
        mGoogleApiClient.disconnect();
    }

    public Location getLastKnownLocation(){
        Location location = new Location("GPS");
        location.setLatitude(14.5517842);
        location.setLongitude(121.0164074);
        return location;
//        return LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }
}
