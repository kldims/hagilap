package com.hagilap.android.hagilap.home.view;

/**
 * Created by kdimla on 4/16/16.
 */
public interface MapView {
    void plotCenter(double lng, double lat);

    void plotUsers(double lng, double lat);

    void refreshCenter(double lng, double lat);
}
