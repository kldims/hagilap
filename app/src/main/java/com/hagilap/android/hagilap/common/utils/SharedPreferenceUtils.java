package com.hagilap.android.hagilap.common.utils;

import com.hagilap.android.hagilap.common.HagilapApplication;

/**
 * Created by kdimla on 4/16/16.
 */
public class SharedPreferenceUtils {

    private static final String LAST_LONGITUDE = "last_long";
    private static final String LAST_LATITUDE = "last_lat";
    private static final String GCM_TOKEN = "gcm_token";
    private static final String HAGILAP_TOKEN = "hagilap_token";
    private static final String HAGILAP_NAME = "hagilap_name";
    private static final String HAGILAP_MSISDN = "hagilap_msisdn";

    public static String getLastLongitude() {
        return HagilapApplication.getmSharedPreferenceManager().getString(LAST_LONGITUDE, "");
    }

    public static void setLastLongitude(String secret) {
        HagilapApplication.getmSharedPreferenceManager().putString(LAST_LONGITUDE, secret);
    }

    public static String getLastLatitude() {
        return HagilapApplication.getmSharedPreferenceManager().getString(LAST_LATITUDE, "");
    }

    public static void setLastLatitude(String secret) {
        HagilapApplication.getmSharedPreferenceManager().putString(LAST_LATITUDE, secret);
    }

    public static String getGcmToken() {
        return HagilapApplication.getmSharedPreferenceManager().getString(GCM_TOKEN, "");
    }

    public static void setGcmToken(String secret) {
        HagilapApplication.getmSharedPreferenceManager().putString(GCM_TOKEN, secret);
    }

    public static String getHagilapToken() {
//      return "b83b4a6d917e16ade042545a109d3a82204aacfde133cb4e847da1b5b241db61181b4781e60c785dfa357cf953509cc07aa44ffa8b2d26734e772ac17f8efb77ec3d4decee6f41a4d53348dd673d0c6e2ba9ec86dc439f02c624474524d0ec3f2c"; //arisa
        return "b83b4a6d917e16ade012575a109d3a82204aacfde133cb4e847df9b5b241db61181b4781e60c785dfa357cf9545a9cc07aa44ffa8b2d26734e772ac17f8efb77ec3d4de7e86441a4d53348dd673d0c6e2ba9ec86dc439f02c62447452fd6e73f2c";//KL

    }

    public static void setHagilapToken(String secret) {
        HagilapApplication.getmSharedPreferenceManager().putString(HAGILAP_TOKEN, secret);
    }

    public static String getHagilapName() {
        return HagilapApplication.getmSharedPreferenceManager().getString(HAGILAP_NAME, "");
    }

    public static void setHagilapName(String secret) {
        HagilapApplication.getmSharedPreferenceManager().putString(HAGILAP_NAME, secret);
    }

    public static String getHagilapMsisdn() {
        return HagilapApplication.getmSharedPreferenceManager().getString(HAGILAP_MSISDN, "");
    }

    public static void getHagilapMsisdn(String secret) {
        HagilapApplication.getmSharedPreferenceManager().putString(HAGILAP_MSISDN, secret);
    }
}
