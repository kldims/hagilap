package com.hagilap.android.hagilap.common.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.hagilap.android.hagilap.common.font.Font;

/**
 * Created by kdimla on 4/17/16.
 */
public class AvenirTextView extends TextView {

    public AvenirTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * When Android sets the typeface, intercept it and use the fonts declared
     * in the application instead. This works with type styles (bold, italic)
     * declared via XML as well.
     */
    @Override
    public void setTypeface(Typeface typeface, int style) {
        switch (style) {
            case Typeface.BOLD:
                super.setTypeface(Font.AVENIR_TYPEFACE);
                break;
            default:
                super.setTypeface(Font.AVENIR_TYPEFACE);
                break;
        }
    }
}


