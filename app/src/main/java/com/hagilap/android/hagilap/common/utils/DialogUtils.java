package com.hagilap.android.hagilap.common.utils;

import android.app.Dialog;

/**
 * Created by kdimla on 4/16/16.
 */
public class DialogUtils {
    public static void showDialog(Dialog dialog) {
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    public static void cancelDialog(Dialog dialog) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public static boolean isShowing(Dialog dialog) {
        if (dialog != null) {
            return dialog.isShowing();
        }
        return false;
    }

}
