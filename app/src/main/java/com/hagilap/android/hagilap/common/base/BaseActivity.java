package com.hagilap.android.hagilap.common.base;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.common.utils.DialogUtils;
import com.hagilap.android.hagilap.common.utils.EventbusUtils;
import com.hagilap.android.hagilap.common.widget.AvenirTextView;
import com.hagilap.android.hagilap.data.service.event.Request;

/**
 * Created by kdimla on 4/16/16.
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseContainer {
    protected ProgressDialog mLoadingDialog;
    protected Toolbar mToolbar;
    AvenirTextView   mToolBarTextView;
    ImageView mBurger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDialogs();
        EventbusUtils.register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventbusUtils.unregister(this);
        DialogUtils.cancelDialog(mLoadingDialog);
    }

    @Override
    public void showLoadingDialog(String title, String message) {
        mLoadingDialog.setTitle(title);
        mLoadingDialog.setMessage(message);
        DialogUtils.showDialog(mLoadingDialog);
    }

    @Override
    public void hideLoadingDialog() {
        DialogUtils.cancelDialog(mLoadingDialog);
    }

    @Override
    public Resources getAppResources() {
        return this.getResources();
    }

    @Override
    public FragmentManager getBaseFragmentManager() {
        return this.getSupportFragmentManager();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void postRequest(Request request) {
        EventbusUtils.postRequest(request);
    }

    @Override
    public Application getApp() {
        return getApplication();
    }

    @Override
    public void startNewActivity(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void showHome(boolean show) {
        if(show){
            mBurger.setImageResource(R.drawable.hamburger);
        } else{
            mBurger.setImageResource(R.drawable.back);
        }
    }

    @Override
    public void showToolBar(boolean show) {
        if(show) {
            mToolbar.setVisibility(View.VISIBLE);
        }else{
            mToolbar.setVisibility(View.GONE);
        }
    }

    @Override
    public void setToolBarTitle(String title) {
        mToolBarTextView.setText(title);
    }


    private void initializeDialogs() {
        mLoadingDialog = new ProgressDialog(this);
    }

    protected void initializeActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.hagilap_common_toolbar);
        mToolBarTextView = (AvenirTextView) mToolbar.findViewById(R.id.hagilap_page_title);
        mBurger = (ImageView)mToolbar.findViewById(R.id.hagilap_burger);
        setSupportActionBar(mToolbar);
    }

}
