package com.hagilap.android.hagilap.data.api.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kdimla on 4/16/16.
 */
public class RegisterResponse {
    @SerializedName("name")
    private String name;
    @SerializedName("msisdn")
    private String msisdn;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
}
