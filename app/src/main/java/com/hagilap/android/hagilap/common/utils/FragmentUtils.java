package com.hagilap.android.hagilap.common.utils;

import android.support.v4.app.Fragment;

import com.hagilap.android.hagilap.common.base.BaseContainer;

/**
 * Created by kdimla on 4/16/16.
 */
public class FragmentUtils {
    public static Fragment findFragmentById(BaseContainer baseContainer, int id) {
        return baseContainer.getBaseFragmentManager().findFragmentById(id);
    }

    public static String createTag(Fragment fragment) {
        return fragment.getClass().getCanonicalName();
    }

    public static void addFragment(BaseContainer baseContainer, int resId, Fragment fragment) {
        baseContainer.getBaseFragmentManager().beginTransaction()
                .add(resId, fragment, createTag(fragment))
                .commit();
    }

    public static void addFragment(BaseContainer baseContainer, int resId, Fragment fragment, int enter) {
        baseContainer.getBaseFragmentManager().beginTransaction()
                .setCustomAnimations(enter, 0)
                .add(resId, fragment, createTag(fragment))
                .commit();
    }

    public static void replaceFragment(BaseContainer baseContainer, int resId, Fragment fragment) {
        baseContainer.getBaseFragmentManager().beginTransaction()
                .replace(resId, fragment, createTag(fragment))
                .commit();
    }

    public static void replaceFragmentAddToBackStack(BaseContainer baseContainer, int resId, Fragment fragment) {
        baseContainer.getBaseFragmentManager().beginTransaction()
                .replace(resId, fragment, createTag(fragment))
                .addToBackStack(createTag(fragment))
                .commit();
    }

    public static void replaceFragment(BaseContainer baseContainer, int resId, Fragment fragment, int enter, int exit) {
        baseContainer.getBaseFragmentManager().beginTransaction()
                .setCustomAnimations(enter, exit)
                .replace(resId, fragment, createTag(fragment))
                .commit();
    }

    public static void removeFragment(BaseContainer baseContainer, Fragment fragment) {
        baseContainer.getBaseFragmentManager().beginTransaction()
                .remove(fragment)
                .commit();
    }

    public static void removeFragment(BaseContainer baseContainer, Fragment fragment, int exit) {
        baseContainer.getBaseFragmentManager().beginTransaction()
                .setCustomAnimations(0, exit)
                .remove(fragment)
                .commit();
    }

    public static void hideFragment(BaseContainer baseContainer, Fragment fragment) {
        baseContainer.getBaseFragmentManager().beginTransaction()
                .hide(fragment)
                .commit();
    }

    public static void hideFragment(BaseContainer baseContainer, Fragment fragment, int exit) {
        baseContainer.getBaseFragmentManager().beginTransaction()
                .setCustomAnimations(0, exit)
                .hide(fragment)
                .commit();
    }
}
