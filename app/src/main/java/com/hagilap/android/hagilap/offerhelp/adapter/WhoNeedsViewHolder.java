package com.hagilap.android.hagilap.offerhelp.adapter;

import android.view.View;
import android.widget.Button;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.common.widget.AvenirTextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by kdimla on 4/17/16.
 */
public class WhoNeedsViewHolder {

    @Bind(R.id.hagilap_whoneeds_name)
    AvenirTextView mName;
    @Bind(R.id.hagilap_whoneeds_distance)
    AvenirTextView mDistance;
    @Bind(R.id.hagilap_whoneeds_descr)
    AvenirTextView mDescription;
    @Bind(R.id.hagilap_whoneeds_help)
    AvenirTextView mHelp;

    public WhoNeedsViewHolder(View view) {
        ButterKnife.bind(this, view);
    }

    public AvenirTextView getmName() {
        return mName;
    }

    public void setmName(AvenirTextView mName) {
        this.mName = mName;
    }

    public AvenirTextView getmDistance() {
        return mDistance;
    }

    public void setmDistance(AvenirTextView mDistance) {
        this.mDistance = mDistance;
    }

    public AvenirTextView getmDescription() {
        return mDescription;
    }

    public void setmDescription(AvenirTextView mDescription) {
        this.mDescription = mDescription;
    }

    public AvenirTextView getmHelp() {
        return mHelp;
    }

    public void setmHelp(AvenirTextView mHelp) {
        this.mHelp = mHelp;
    }
}
