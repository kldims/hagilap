package com.hagilap.android.hagilap.match.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.common.base.BaseActivity;
import com.hagilap.android.hagilap.common.constants.RequestCode;
import com.hagilap.android.hagilap.common.utils.FragmentUtils;
import com.hagilap.android.hagilap.data.gcm.model.AcceptOfferPush;
import com.hagilap.android.hagilap.data.gcm.model.InquiryPush;
import com.hagilap.android.hagilap.data.service.event.Request;
import com.hagilap.android.hagilap.data.service.event.RequestError;
import com.hagilap.android.hagilap.data.service.event.RequestSuccess;
import com.hagilap.android.hagilap.match.fragment.MatchFragment;

import org.greenrobot.eventbus.Subscribe;

import timber.log.Timber;

/**
 * Created by kdimla on 4/16/16.
 */
public class MatchActivity extends BaseActivity implements MatchContainer {
    private MatchFragment matchFragment;
    private String inquiryId;
    private String offferId;
    private boolean isAccepted=true;

    @Override
    public void setToolBarTitle(String title) {

    }

    @Override
    public void showToolBar(boolean show) {

    }

    @Override
    public View getBaseView() {
        return null;
    }

    @Subscribe
    public void onEventMainThread(Request request) {

    }

    @Subscribe
    public void onEventMainThread(RequestSuccess requestSuccess) {
        switch (requestSuccess.getmRequestCode()) {
            case RequestCode.GCM_MSG_RECEIVED:
                if (requestSuccess.getmResponseObject() instanceof AcceptOfferPush) {
                    if (((InquiryPush) requestSuccess.getmResponseObject()).getType().equals("ACCEPTED")) {
                        hideLoadingDialog();
                    }
                }
                break;

        }
    }

    @Subscribe
    public void onEventMainThread(RequestError requestError) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);


        setContentView(R.layout.hagilap_activity_match);
        inquiryId = getIntent().getStringExtra("inquiry");
        offferId = getIntent().getStringExtra("offer");
        isAccepted = getIntent().getBooleanExtra("accepted", true);
        Timber.i("KL DIMS::: inquiry: " + inquiryId);
        Timber.i("KL DIMS::: inquiry: " + offferId);
        initialize();
    }

    @Override
    public void call(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:09473371427"));
        startActivity(intent);
    }

    @Override
    public void sms(String number) {
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", "09473371427");
        startActivity(smsIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private void initialize() {
        matchFragment = MatchFragment.newInstance(inquiryId, offferId);
        if(!isAccepted) {
            showLoadingDialog("You just offered a help!","Waiting for confirmation...");
        }
        FragmentUtils.replaceFragment(this, R.id.hagilap_match_holder,matchFragment);
    }
}

