package com.hagilap.android.hagilap.askhelp.options.adapter;

import android.view.View;
import android.widget.TextView;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.common.widget.AvenirTextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by kdimla on 4/16/16.
 */
public class OptionViewHolder {
    @Bind(R.id.hagilap_option_item_name)
    AvenirTextView optionName;
    public OptionViewHolder(View view){
        ButterKnife.bind(this, view);
    }

    public AvenirTextView getOptionName() {
        return optionName;
    }

    public void setOptionName(AvenirTextView optionName) {
        this.optionName = optionName;
    }
}
