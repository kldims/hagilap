package com.hagilap.android.hagilap.common.base;

import android.support.v4.app.Fragment;

import com.hagilap.android.hagilap.data.service.event.RequestError;
import com.hagilap.android.hagilap.data.service.event.RequestSuccess;

/**
 * Created by kdimla on 4/16/16.
 */
public abstract class BaseFragment extends Fragment {

    public abstract void onReceiveSuccess(RequestSuccess requestSuccess);

    public abstract void onReceiveError(RequestError requestError);
}
