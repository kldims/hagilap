package com.hagilap.android.hagilap.common.constants;

/**
 * Created by kdimla on 4/17/16.
 */
public interface Category {
    String HOSPITAL = "EMERGENCY";
    String NOT_GOOD = "MEDICINE";
    String EQUIPMENT = "EQUIPMENT";
    String MEDREP = "EMERGENCY";
}
