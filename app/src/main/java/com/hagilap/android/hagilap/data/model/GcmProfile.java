package com.hagilap.android.hagilap.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kdimla on 4/16/16.
 */
public class GcmProfile {
    @SerializedName("gcmToken")
    private String gcmToken;
    @SerializedName("location")
    private Location location;

    public String getGcmToken() {
        return gcmToken;
    }

    public void setGcmToken(String gcmToken) {
        this.gcmToken = gcmToken;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
