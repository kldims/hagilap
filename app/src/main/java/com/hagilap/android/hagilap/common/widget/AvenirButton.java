package com.hagilap.android.hagilap.common.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.hagilap.android.hagilap.common.font.Font;

/**
 * Created by kdimla on 4/17/16.
 */
public class AvenirButton  extends Button {

    public AvenirButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setTypeface(Typeface typeface, int style) {
        switch (style) {
            case Typeface.BOLD:
                super.setTypeface(Font.AVENIR_TYPEFACE);
                break;
            default:
                super.setTypeface(Font.AVENIR_TYPEFACE);
                break;
        }
    }
}