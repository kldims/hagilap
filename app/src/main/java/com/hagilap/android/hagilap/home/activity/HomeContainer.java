package com.hagilap.android.hagilap.home.activity;

import com.hagilap.android.hagilap.common.base.BaseContainer;

/**
 * Created by kdimla on 4/16/16.
 */
public interface HomeContainer extends BaseContainer {
    void toggleDrawer();
    void showNeedClinic();
    void showNotGood();
    void showMedEquipment();
    void showAskMedRep();
    void showWhoNeedsHelp();
    void showAskForHelp();
}
