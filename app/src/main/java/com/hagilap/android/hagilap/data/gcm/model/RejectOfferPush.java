package com.hagilap.android.hagilap.data.gcm.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kdimla on 4/16/16.
 */
public class RejectOfferPush extends InquiryPush {
    @SerializedName("offerId")
    private String offerid;

    @SerializedName("status")
    private String status;

    public String getOfferid() {
        return offerid;
    }

    public void setOfferid(String offerid) {
        this.offerid = offerid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
