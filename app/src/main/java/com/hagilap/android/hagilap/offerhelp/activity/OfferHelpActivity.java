package com.hagilap.android.hagilap.offerhelp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.LinearLayout;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.common.base.BaseActivity;
import com.hagilap.android.hagilap.common.constants.RequestCode;
import com.hagilap.android.hagilap.common.utils.EventbusUtils;
import com.hagilap.android.hagilap.common.utils.FragmentUtils;
import com.hagilap.android.hagilap.common.utils.SharedPreferenceUtils;
import com.hagilap.android.hagilap.data.api.response.OfferResponse;
import com.hagilap.android.hagilap.data.gcm.model.AcceptOfferPush;
import com.hagilap.android.hagilap.data.gcm.model.InquiryPush;
import com.hagilap.android.hagilap.data.service.event.Request;
import com.hagilap.android.hagilap.data.service.event.RequestError;
import com.hagilap.android.hagilap.data.service.event.RequestSuccess;
import com.hagilap.android.hagilap.match.activity.MatchActivity;
import com.hagilap.android.hagilap.offerhelp.fragment.OfferHelpFragment;

import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by kdimla on 4/17/16.
 */
public class OfferHelpActivity extends BaseActivity implements OfferHelpContainer {
    OfferHelpFragment offerHelpFragment;
    @Bind(R.id.hagilap_match_holder)
    CoordinatorLayout linearLayout;

    private String inquiryid;
    private String offerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hagilap_activity_match);
        ButterKnife.bind(this);
        initialize();
        initializeActionBar();
        setToolBarTitle("Who needs help?");
        showHome(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void setToolBarTitle(String title) {
        super.setToolBarTitle(title);
    }

    @Override
    public void showToolBar(boolean show) {

    }

    @Override
    public void showMatch(String offerid, String inquiryId) {
        Intent intent = new Intent(this, MatchActivity.class);
        intent.putExtra("offer", offerid);
        intent.putExtra("inquiry", inquiryId);
        intent.putExtra("accepted", true);
        startActivity(intent);
        finish();

    }

    @Override
    public void setInquiryId(String inquiryId) {
        inquiryid = inquiryId;
    }

    @Override
    public View getBaseView() {
        return linearLayout;
    }


    @Subscribe
    public void onEventMainThread(Request request) {

    }


    @Subscribe
    public void onEventMainThread(RequestSuccess requestSuccess) {
        switch (requestSuccess.getmRequestCode()) {
            case RequestCode.LOCAL_GET_INQUIRIES:
                hideLoadingDialog();
                offerHelpFragment.onReceiveSuccess(requestSuccess);
                break;
            case RequestCode.GCM_MSG_RECEIVED:
                Timber.i("KL DIMS::: k0");
                if (((InquiryPush) requestSuccess.getmResponseObject()).getType()!=null) {
                    Timber.i("KL DIMS::: k1");
                    if (((InquiryPush) requestSuccess.getmResponseObject()).getType().equals("INQUIRY")) {
                        Timber.i("KL DIMS::: skjfhsdjfhds");
                        offerHelpFragment.onReceiveSuccess(requestSuccess);
                    } else if(((AcceptOfferPush) requestSuccess.getmResponseObject()).getStatus().equals("ACCEPTED")){
                        if(offerId == null){
                            offerId = SharedPreferenceUtils.getHagilapMsisdn();
                        }
                        if(inquiryid == null){
                            inquiryid = SharedPreferenceUtils.getHagilapName();
                        }
                        showMatch(offerId, inquiryid);
                    }
                }
                break;
            case RequestCode.API_CREATE_OFFER: {
                hideLoadingDialog();
                offerId = ((OfferResponse) requestSuccess.getmResponseObject()).getOfferId();
                SharedPreferenceUtils.getHagilapMsisdn(offerId);
                showMatch(offerId, inquiryid);
                break;
            }
        }
    }

    @Subscribe
    public void onEventMainThread(RequestError requestError) {

    }

    private void initialize() {
        offerHelpFragment = OfferHelpFragment.newInstance();
        showLoadingDialog("What a good day to help.", "Getting inquiries...");
        FragmentUtils.replaceFragment(this, R.id.hagilap_match_holder, offerHelpFragment);
    }
}
