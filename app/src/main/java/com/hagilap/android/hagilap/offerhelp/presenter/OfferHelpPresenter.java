package com.hagilap.android.hagilap.offerhelp.presenter;

import com.hagilap.android.hagilap.data.gcm.model.InquiryPush;

import java.util.List;

/**
 * Created by kdimla on 4/17/16.
 */
public interface OfferHelpPresenter {
    List<InquiryPush> getOffers();

    void onHelp(int position);

    void setInquiries(List<InquiryPush> inquiries);

    void addInquiries(InquiryPush inquiry);
}
