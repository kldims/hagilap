package com.hagilap.android.hagilap.match.activity;

import com.hagilap.android.hagilap.common.base.BaseContainer;

/**
 * Created by kdimla on 4/17/16.
 */
public interface MatchContainer extends BaseContainer {
    void call(String number);
    void sms(String number);
}
