package com.hagilap.android.hagilap.offerhelp.activity;

import com.hagilap.android.hagilap.common.base.BaseContainer;

/**
 * Created by kdimla on 4/17/16.
 */
public interface OfferHelpContainer extends BaseContainer {
    void showMatch(String offerid, String inquiryId);
    void setInquiryId(String inquiryId);
}
