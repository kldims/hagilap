package com.hagilap.android.hagilap.askhelp.options.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.askhelp.activity.AskContainer;
import com.hagilap.android.hagilap.askhelp.options.adapter.OptionsAdapter;
import com.hagilap.android.hagilap.askhelp.options.presenter.OptionsPresenter;
import com.hagilap.android.hagilap.askhelp.options.presenter.impl.OptionsPresenterImpl;
import com.hagilap.android.hagilap.askhelp.options.view.OptionsView;
import com.hagilap.android.hagilap.common.base.BaseFragment;
import com.hagilap.android.hagilap.common.constants.RequestCode;
import com.hagilap.android.hagilap.data.api.response.CreateInquiryResponse;
import com.hagilap.android.hagilap.data.service.event.RequestError;
import com.hagilap.android.hagilap.data.service.event.RequestSuccess;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by kdimla on 4/16/16.
 */
public class OptionsFragment extends BaseFragment implements OptionsView {
    private AskContainer mBaseContainer;
    private OptionsPresenter mOptionsPresenter;

    @Bind(R.id.hagilap_options_listview)
    ListView optionsListView;

    private OptionsAdapter mOptionsAdapter;
    private int action;

    public static OptionsFragment newInstance() {
        OptionsFragment mapFragment = new OptionsFragment();
        Bundle arguments = new Bundle();
        mapFragment.setArguments(arguments);
        return mapFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hagilap_fragment_ask_options, container, false);
        ButterKnife.bind(this, view);
        initialize();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mBaseContainer = (AskContainer) getActivity();
    }

    @Override
    public void onReceiveSuccess(RequestSuccess requestSuccess) {
        switch (requestSuccess.getmRequestCode()){
            case RequestCode.API_CREATE_INQUIRY:
                mBaseContainer.showGetOffersFragment();
                mBaseContainer.showLoadingDialog("Find help","Waiting for helping hands...");
                break;
        }
    }

    @Override
    public void onReceiveError(RequestError requestError) {

    }


    public void setAction(int action) {
        this.action = action;
    }

    private void initialize() {
        Timber.i("KL DIMS::: init");
        mOptionsPresenter = new OptionsPresenterImpl(mBaseContainer, this);
        optionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mOptionsPresenter.doOnOptionClick(position);
            }
        });

        mOptionsAdapter = new OptionsAdapter(mBaseContainer.getContext(), mOptionsPresenter.getOptions(action));
        optionsListView.setAdapter(mOptionsAdapter);
    }

}