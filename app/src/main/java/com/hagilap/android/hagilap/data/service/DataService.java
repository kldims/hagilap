package com.hagilap.android.hagilap.data.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.hagilap.android.hagilap.common.constants.RequestCode;
import com.hagilap.android.hagilap.common.utils.BackgroundExecutor;
import com.hagilap.android.hagilap.common.utils.EventbusUtils;
import com.hagilap.android.hagilap.common.utils.SharedPreferenceUtils;
import com.hagilap.android.hagilap.common.utils.SnackbarUtils;
import com.hagilap.android.hagilap.data.api.HagilapApi;
import com.hagilap.android.hagilap.data.api.response.CreateInquiryResponse;
import com.hagilap.android.hagilap.data.api.response.Default;
import com.hagilap.android.hagilap.data.api.response.OfferResponse;
import com.hagilap.android.hagilap.data.gcm.model.InquiryPush;
import com.hagilap.android.hagilap.data.gcm.utils.GcmUtils;
import com.hagilap.android.hagilap.data.model.AcceptedOffer;
import com.hagilap.android.hagilap.data.service.event.Request;
import com.hagilap.android.hagilap.data.service.event.RequestError;
import com.hagilap.android.hagilap.data.service.event.RequestSuccess;
import com.jakewharton.retrofit.Ok3Client;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by kdimla on 4/16/16.
 */
public class DataService extends Service {
    private HagilapApi mHagilapApi;
    private List<InquiryPush> inquiryPushList = new ArrayList<>();
    private static final String BASE_API = "http://52.221.252.58:8085/v1";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        EventbusUtils.register(this);
        initializeAPI();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventbusUtils.unregister(this);
    }


    @Subscribe
    public void onEventMainThread(final Request request) {
        Timber.i("KL DIMS::: On event: " + request.getRequestCode());
        switch (request.getRequestCode()) {
            case RequestCode.API_REGISTRATION:
            case RequestCode.API_RESEND:
                break;
            case RequestCode.API_VERIFY:
                break;
            case RequestCode.API_GET_PROFILE:
                break;
            case RequestCode.API_UPDATE_PROFILE:
                break;
            case RequestCode.API_GET_MERCHANTS:
                break;
            case RequestCode.API_GET_DEALS:
                break;
            case RequestCode.API_CREATE_INQUIRY:
                mHagilapApi.poll(request.getBodyAsTypedOutput(), new Callback<CreateInquiryResponse>() {
                    @Override
                    public void success(CreateInquiryResponse createInquiryResponse, Response response) {
                        Timber.i("KL DIMS::: respo inq: "+createInquiryResponse.getInquiryid());
                        SharedPreferenceUtils.setHagilapName(createInquiryResponse.getInquiryid());
                        RequestSuccess<CreateInquiryResponse> requestSuccess = new RequestSuccess<>(request);
                        requestSuccess.setmResponseObject(createInquiryResponse);
                        EventbusUtils.postRequest(requestSuccess);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        processError(request, error);
                    }
                });
                break;
            case RequestCode.API_ACCEPT_INQUIRY:
                mHagilapApi.resolvePoll((String) request.getParameters().get("inquiry"), new Callback<Default>() {
                    @Override
                    public void success(Default defaults, Response response) {
                        RequestSuccess requestSuccess = new RequestSuccess(request);
                        EventbusUtils.postRequest(requestSuccess);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        processError(request, error);
                    }
                });
                break;
            case RequestCode.API_CREATE_OFFER:
                mHagilapApi.offer((String) request.getParameters().get("inquiry"),
                        request.getBodyAsTypedOutput(),
                        new Callback<OfferResponse>() {
                            @Override
                            public void success(OfferResponse offer, Response response) {

                                Timber.i("KL DIMS::: offer+");
                                RequestSuccess<OfferResponse> requestSuccess = new RequestSuccess<>(request);
                                requestSuccess.setmResponseObject(offer);
                                EventbusUtils.postRequest(requestSuccess);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                processError(request, error);
                            }
                        });
                break;
            case RequestCode.API_ACCEPT_OFFER:
                mHagilapApi.acceptOffer((String) request.getParameters().get("inquiry"),
                        (String) request.getParameters().get("offer"),
                        new Callback<AcceptedOffer>() {
                            @Override
                            public void success(AcceptedOffer acceptedOffer, Response response) {

                                RequestSuccess<AcceptedOffer> requestSuccess = new RequestSuccess<>(request);
                                requestSuccess.setmResponseObject(acceptedOffer);
                                EventbusUtils.postRequest(requestSuccess);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                processError(request, error);
                            }
                        });
                break;
            //GCM REGISTER
            case RequestCode.GCM_REGISTER:
                BackgroundExecutor.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        String token = GcmUtils.handleGcmIntent(DataService.this);
                        if (!TextUtils.isEmpty(token)) {
                            SharedPreferenceUtils.setGcmToken(token);
                            RequestSuccess<String> requestSuccess = new RequestSuccess<String>(request);
                            requestSuccess.setmResponseObject(token);
                            EventbusUtils.postRequest(requestSuccess);
                        } else {
                            RequestError requestError = new RequestError(com.hagilap.android.hagilap.common.constants.Error.GCM_ERROR, request);
                            EventbusUtils.postRequest(requestError);
                        }
                    }
                });
                break;
            case RequestCode.GCM_MSG_RECEIVED:
                BackgroundExecutor.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        String message = (String) request.getmObject();
                        Object object = GcmUtils.parseMessage(message);
                        RequestSuccess requestSuccess = new RequestSuccess(request);
                        requestSuccess.setmResponseObject(object);
                        ifInquiry(requestSuccess);
                        EventbusUtils.postRequest(requestSuccess);
                    }
                });
                break;
            case RequestCode.LOCAL_GET_INQUIRIES:
                RequestSuccess requestSuccess = new RequestSuccess(request);
                requestSuccess.setmResponseObject(inquiryPushList);
                EventbusUtils.postRequest(requestSuccess);
                break;
        }
    }

    @Subscribe
    public void onEvent(final RequestSuccess requestSuccess) {
        switch (requestSuccess.getmRequestCode()) {
            case RequestCode.GCM_REGISTER:
                String token = (String) requestSuccess.getmResponseObject();
                //TODO: save to prefs
                break;
        }
    }

    @Subscribe
    public void onEvent(final RequestError requestError) {
        switch (requestError.getmRequestCode()) {
            case RequestCode.GCM_REGISTER:
                //TODO: save to prefs
                break;
        }
    }

    private void ifInquiry(RequestSuccess requestSuccess){
        if(requestSuccess.getmResponseObject()!= null) {
            Timber.i("KL DIMS::: 0");
            if (((InquiryPush) requestSuccess.getmResponseObject()).getType() != null) {
                Timber.i("KL DIMS::: 1");
                if (((InquiryPush) requestSuccess.getmResponseObject()).getType().equals("INQUIRY")) {
                    Timber.i("KL DIMS::: kkk11");
                    inquiryPushList.add(((InquiryPush) requestSuccess.getmResponseObject()));
                }
            }
        }

    }

    private void initializeAPI() {
        Ok3Client client = new Ok3Client(new OkHttpClient());

        mHagilapApi = new RestAdapter.Builder()
                .setClient(client)
                .setEndpoint(BASE_API)
                .setExecutors(BackgroundExecutor.getInstance(),
                        BackgroundExecutor.getInstance())
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("token", SharedPreferenceUtils.getHagilapToken());
                    }
                })
                .build().create(HagilapApi.class);

        getRegToken();
    }

    private void getRegToken() {
        EventbusUtils.postRequest(new Request(RequestCode.GCM_REGISTER));
    }

    private void processError(Request request, RetrofitError error) {
        RequestError requestError = new RequestError(com.hagilap.android.hagilap.common.constants.Error.API_ERROR, request);
        EventbusUtils.postRequest(requestError);
    }
}
