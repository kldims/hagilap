package com.hagilap.android.hagilap.data.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kdimla on 4/16/16.
 */
public class Profile {
    @SerializedName("name")
    private String name;
    @SerializedName("msisdn")
    private String number;
    @SerializedName("age")
    private int age;
    @SerializedName("gender")
    private String gender;
    @SerializedName("medPrac")
    private boolean isMedPrac;
    @SerializedName("points")
    private int points;
    @Nullable
    @SerializedName("location")
    private Location location;
    @SerializedName("userType")
    private String userType;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isMedPrac() {
        return isMedPrac;
    }

    public void setIsMedPrac(boolean isMedPrac) {
        this.isMedPrac = isMedPrac;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
