package com.hagilap.android.hagilap.common.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by kdimla on 4/16/16.
 */
public class NetworkUtils {
    public static boolean isConnectedToNetwork(Context context) {
        ConnectivityManager connectivityManager = null;
        NetworkInfo networkInfo = null;

        if (context != null) {
            connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
        }

        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }

        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }
}
