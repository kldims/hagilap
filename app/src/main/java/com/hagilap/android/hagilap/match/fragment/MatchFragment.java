package com.hagilap.android.hagilap.match.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.common.HagilapApplication;
import com.hagilap.android.hagilap.common.base.BaseFragment;
import com.hagilap.android.hagilap.common.widget.AvenirTextView;
import com.hagilap.android.hagilap.data.service.event.RequestError;
import com.hagilap.android.hagilap.data.service.event.RequestSuccess;
import com.hagilap.android.hagilap.match.activity.MatchContainer;
import com.hagilap.android.hagilap.match.presenter.MatchPresenter;
import com.hagilap.android.hagilap.match.presenter.impl.MatchPresenterImpl;
import com.hagilap.android.hagilap.match.view.MatchView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by kdimla on 4/16/16.
 */
public class MatchFragment extends BaseFragment implements MatchView {
    private MatchContainer mBaseContainer;
    private MatchPresenter mMatchPresenter;

    private GoogleMap mMap;
    private Marker mCenterMarker;
    private Marker mOtherMarker;

    @Bind(R.id.hagilap_match_map)
    MapView mapView;
    @Bind(R.id.hagilap_match_name)
    AvenirTextView mName;
    @Bind(R.id.hagilap_match_distance)
    AvenirTextView mDistance;
    @Bind(R.id.hagilap_match_descr)
    AvenirTextView mDescr;

    public static MatchFragment newInstance(String inquiry, String offer) {
        MatchFragment matchFragment = new MatchFragment();
        Bundle arguments = new Bundle();
        arguments.putString("inquiry", inquiry);
        arguments.putString("offer", offer);
        matchFragment.setArguments(arguments);
        return matchFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hagilap_fragment_match, container, false);
        ButterKnife.bind(this, view);
        initialize(savedInstanceState);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mBaseContainer = (MatchContainer) getActivity();
    }

    @Override
    public void onReceiveSuccess(RequestSuccess requestSuccess) {

    }

    @Override
    public void onReceiveError(RequestError requestError) {

    }

    @Override
    public void setHelperName(String name) {
        mName.setText(name);
    }

    @Override
    public void setHelperDescription(String description) {
        mDescr.setText(description);
    }

    @Override
    public void setHelperDistance(String distance) {
        mDistance.setText(distance);
    }

    @OnClick(R.id.hagilap_match_call)
    public void call() {
        mMatchPresenter.call();
    }

    @OnClick(R.id.hagilap_match_sms)
    public void sms() {
        mMatchPresenter.sms();
    }

    @OnClick(R.id.hagilap_match_cancel)
    public void cancel() {

    }

    @OnClick(R.id.hagilap_match_resolve)
    public void resolve() {
        mMatchPresenter.resolve();
    }

    @Override
    public void plotSelf(double lng, double lat) {
        if (mCenterMarker != null) {
            mCenterMarker.remove();
        }
        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
        markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.me));
        mCenterMarker = mMap.addMarker(markerOption);
    }

    @Override
    public void plotOther(double lng, double lat) {
        if (mOtherMarker != null) {
            mOtherMarker.remove();
        }
        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
        markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.hagilaper));
        mOtherMarker = mMap.addMarker(markerOption);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
        if (mMatchPresenter != null) {
            refreshCenter(HagilapApplication.getmGoogleApiManager().getLastKnownLocation().getLongitude(),
                    HagilapApplication.getmGoogleApiManager().getLastKnownLocation().getLatitude());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    public void refreshCenter(double lng, double lat) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(lat,
                lng), 15);
        if (mMap != null) {
            mMap.animateCamera(cameraUpdate);
//            plotCenter(lng, lat);
        }
    }

    private void initialize(Bundle ubbundle) {
        initializeMap(ubbundle);
    }

    private void initializeMap(Bundle bundle) {
        mapView.onCreate(bundle);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.getUiSettings().setCompassEnabled(true);
                mMatchPresenter = new MatchPresenterImpl(mBaseContainer, MatchFragment.this, getArguments().getString("inquiry"));
                refreshCenter(HagilapApplication.getmGoogleApiManager().getLastKnownLocation().getLongitude(),
                        HagilapApplication.getmGoogleApiManager().getLastKnownLocation().getLatitude());
            }
        });
    }


}
