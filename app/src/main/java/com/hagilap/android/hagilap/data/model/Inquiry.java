package com.hagilap.android.hagilap.data.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kdimla on 4/16/16.
 */
public class Inquiry {
    @SerializedName("item")
    private String item;
    @SerializedName("category")
    private String category;
    @Nullable
    @SerializedName("location")
    private Location location;
    @Nullable
    @SerializedName("id")
    private String id;
    @Nullable
    @SerializedName("status")
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Nullable
    public String getStatus() {
        return status;
    }

    public void setStatus(@Nullable String status) {
        this.status = status;
    }
}
