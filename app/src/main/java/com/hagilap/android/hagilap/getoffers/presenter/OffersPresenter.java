package com.hagilap.android.hagilap.getoffers.presenter;

import com.hagilap.android.hagilap.data.gcm.model.OfferPush;

import java.util.List;

/**
 * Created by kdimla on 4/16/16.
 */
public interface OffersPresenter {
    List<OfferPush> getOffers();
    void refreshOffers();
    void acceptOffer(int position);
    void addNewOffer(OfferPush offerPush);
    void setInquiry(String inquiry);
}
