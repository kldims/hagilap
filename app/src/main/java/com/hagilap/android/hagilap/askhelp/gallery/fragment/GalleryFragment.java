package com.hagilap.android.hagilap.askhelp.gallery.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.askhelp.activity.AskContainer;
import com.hagilap.android.hagilap.askhelp.gallery.view.GalleryView;
import com.hagilap.android.hagilap.common.base.BaseFragment;
import com.hagilap.android.hagilap.common.constants.Action;
import com.hagilap.android.hagilap.data.service.event.RequestError;
import com.hagilap.android.hagilap.data.service.event.RequestSuccess;

import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * Created by kdimla on 4/16/16.
 */
public class GalleryFragment extends BaseFragment implements GalleryView {
    private AskContainer mBaseContainer;

    public static GalleryFragment newInstance() {
        GalleryFragment mapFragment = new GalleryFragment();
        Bundle arguments = new Bundle();
        mapFragment.setArguments(arguments);
        return mapFragment;
    }

    @Override
    public void onReceiveSuccess(RequestSuccess requestSuccess) {

    }

    @Override
    public void onReceiveError(RequestError requestError) {

    }

    @OnClick({R.id.hagilap_gallery_hospital, R.id.hagilap_gallery_notgood, R.id.hagilap_gallery_equipment, R.id.hagilap_gallery_medrep})
    public void onClick(View view) {
        Timber.i("KL DIMS::: clicked!!!");
        switch (view.getId()) {
            case R.id.hagilap_gallery_hospital:
                showOptions(Action.HOSPITAL);
                break;
            case R.id.hagilap_gallery_notgood:
                showOptions(Action.NOT_GOOD);
                break;
            case R.id.hagilap_gallery_equipment:
                showOptions(Action.EQUIPMENT);
                break;
            case R.id.hagilap_gallery_medrep:
                showOptions(Action.MEDREP);
                break;
        }
    }

    @Override
    public void showOptions(int action) {
        mBaseContainer.showOptionsFragment(action);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hagilap_fragment_ask_gallery, container, false);
        ButterKnife.bind(this, view);
        initialize();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mBaseContainer = (AskContainer) getActivity();
    }

    private void initialize() {
    }
}

