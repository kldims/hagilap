package com.hagilap.android.hagilap.data.api.request;

import com.google.gson.Gson;
import com.hagilap.android.hagilap.data.model.Inquiry;
import com.hagilap.android.hagilap.data.model.Location;
import com.hagilap.android.hagilap.data.model.Offer;

/**
 * Created by kdimla on 4/16/16.
 */
public class Body {
    public interface ContentType {
        String JSON = "application/json;charset=utf-8";
    }

    private interface Key {
        String REG_NAME = "name";
        String REG_MSISDN = "msisdn";
        String VERIFY_VCODE = "vcode";
        String VERIFY_MSISDN = "msisdn";
    }

    public static byte[] getCreateInquiry(Inquiry inquiry) {
        Gson gson = new Gson();
        String body = gson.toJson(inquiry);
        return body.getBytes();
    }

    public static byte[] getCreateOffer(Offer offer) {
        Gson gson = new Gson();
        String body = gson.toJson(offer);
        return body.getBytes();
    }

    public static byte[] getAcceptOffer(Location location) {
        Gson gson = new Gson();
        String body = gson.toJson(location);
        return body.getBytes();
    }


}
