package com.hagilap.android.hagilap.home.presenter.impl;

import android.location.Location;

import com.hagilap.android.hagilap.common.HagilapApplication;
import com.hagilap.android.hagilap.common.base.BaseContainer;
import com.hagilap.android.hagilap.home.presenter.MapPresenter;
import com.hagilap.android.hagilap.home.view.MapView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kdimla on 4/16/16.
 */
public class MapPresenterImpl implements MapPresenter {
    private BaseContainer mBaseContainer;
    private MapView mMapView;

    private List<Location> mUsers;

    public MapPresenterImpl(BaseContainer baseContainer, MapView mapView) {
        this.mBaseContainer = baseContainer;
        this.mMapView = mapView;
        mockData();
        plotUsers();
    }

    @Override
    public void plotUsers() {
        mMapView.plotUsers(121.01698675714715, 14.552028239497169);
        mMapView.plotUsers(121.01687678657754, 14.551656987814708);
        mMapView.plotUsers(121.01602116190179, 14.551509006200783);
        mMapView.plotUsers(121.01581731401666, 14.551968527730246);
        mMapView.plotUsers(121.01626524292215, 14.551631026135238);
        mMapView.plotUsers(121.01549813114389, 14.551509006200783);
        mMapView.plotUsers(121.0168875154136, 14.552228143990712);

    }

    @Override
    public void plotCurrentLocation() {
        if (HagilapApplication.getmGoogleApiManager().getLastKnownLocation() != null) {
            android.location.Location location = HagilapApplication.getmGoogleApiManager().getLastKnownLocation();
            mMapView.plotCenter(location.getLongitude(), location.getLatitude());
            mMapView.refreshCenter(location.getLongitude(), location.getLatitude());
        }
    }

    private void mockData() {
        mUsers = new ArrayList<>();
//        Location location = new Location("GPS");
//        location.setLatitude();
//        location.setLongitude();
//        mUsers.add(location);
//
//        location = new Location("GPS");
//        location.setLatitude();
//        location.setLongitude();
//        mUsers.add(location);
//
//        Location location = new Location("GPS");
//        location.setLatitude();
//        location.setLongitude();
//        mUsers.add(location);
//
//        Location location = new Location("GPS");
//        location.setLatitude();
//        location.setLongitude();
//        mUsers.add(location);

    }
}
