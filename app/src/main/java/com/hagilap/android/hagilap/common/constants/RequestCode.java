package com.hagilap.android.hagilap.common.constants;

/**
 * Created by kdimla on 4/16/16.
 */
public interface RequestCode {
    int API_REGISTRATION = 100;
    int API_RESEND = 101;
    int API_VERIFY = 102;
    int API_GET_PROFILE = 103;
    int API_UPDATE_PROFILE = 104;
    int API_GET_MERCHANTS = 105;
    int API_GET_DEALS = 106;
    int API_CREATE_INQUIRY = 107;
    int API_ACCEPT_INQUIRY = 108;
    int API_CREATE_OFFER = 109;
    int API_ACCEPT_OFFER = 110;

    int GCM_REGISTER = 111;
    int GCM_MSG_RECEIVED = 112;

    int LOCAL_GET_INQUIRIES=113;

}
