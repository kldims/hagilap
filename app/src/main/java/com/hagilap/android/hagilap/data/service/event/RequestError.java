package com.hagilap.android.hagilap.data.service.event;

/**
 * Created by kdimla on 4/16/16.
 */
public class RequestError {
    private int mStatus;
    private int mErrorCode;
    private String message;
    private Request mRequest;
    private int mRequestCode;

    public RequestError(int errorcode, Request request) {
        this.mErrorCode = errorcode;
        this.mRequest = request;
        this.mRequestCode = request.getRequestCode();
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getmStatus() {
        return mStatus;
    }

    public int getmErrorCode() {
        return mErrorCode;
    }

    public String getMessage() {
        return message;
    }

    public Request getmRequest() {
        return mRequest;
    }

    public int getmRequestCode() {
        return mRequestCode;
    }
}
