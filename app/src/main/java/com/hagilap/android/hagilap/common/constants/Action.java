package com.hagilap.android.hagilap.common.constants;

/**
 * Created by kdimla on 4/16/16.
 */
public interface Action {
    int HOSPITAL = 500;
    int NOT_GOOD = 501;
    int EQUIPMENT = 502;
    int MEDREP = 503;
}
