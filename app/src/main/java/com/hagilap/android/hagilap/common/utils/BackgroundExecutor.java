package com.hagilap.android.hagilap.common.utils;

import android.support.annotation.NonNull;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Created by kdimla on 4/16/16.
 */
public class BackgroundExecutor implements Executor {
    private static final BackgroundExecutor BACKGROUND_EXECUTOR = new BackgroundExecutor();
    private Executor mExecutor;

    public static BackgroundExecutor getInstance() {
        return BACKGROUND_EXECUTOR;
    }

    private BackgroundExecutor() {
        ThreadFactory mThreadFactory = new ThreadFactory() {
            @Override
            public Thread newThread(@NonNull final Runnable runnable) {
                return new Thread() {
                    @Override
                    public void run() {
                        setPriority(MIN_PRIORITY);
                        runnable.run();
                    }
                };
            }
        };
        mExecutor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(), mThreadFactory);
    }

    @Override
    public void execute(Runnable command) {
        mExecutor.execute(command);
    }
}
