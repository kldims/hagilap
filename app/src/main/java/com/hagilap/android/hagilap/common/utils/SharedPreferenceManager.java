package com.hagilap.android.hagilap.common.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.hagilap.android.hagilap.R;

/**
 * Created by kdimla on 4/16/16.
 */
public class SharedPreferenceManager {
    private static SharedPreferenceManager sInstance;
    private static SharedPreferences sSharedPreferences;
    private static SharedPreferences.Editor sPreferencesEditor;

    public static SharedPreferenceManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SharedPreferenceManager(context);
        }
        return sInstance;
    }

    private SharedPreferenceManager(Context context) {
        String filename = context.getResources().getString(R.string.hagilap_shared_prefs);
        sSharedPreferences = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
        sPreferencesEditor = sSharedPreferences.edit();
    }

    public String getString(String key, String defaultValue) {
        return sSharedPreferences.getString(key, defaultValue);
    }

    public SharedPreferenceManager putString(String key, String value) {
        sPreferencesEditor.putString(key, value);
        sPreferencesEditor.commit();
        return sInstance;
    }

    public int getInt(String key, int defaultValue) {
        return sSharedPreferences.getInt(key, defaultValue);
    }

    public SharedPreferenceManager putInt(String key, int value) {
        sPreferencesEditor.putInt(key, value);
        sPreferencesEditor.commit();
        return sInstance;
    }

    public Long getLong(String key, Long defaultValue) {
        return sSharedPreferences.getLong(key, defaultValue);
    }

    public SharedPreferenceManager putLong(String key, Long value) {
        sPreferencesEditor.putLong(key, value);
        sPreferencesEditor.commit();
        return sInstance;
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return sSharedPreferences.getBoolean(key, defaultValue);
    }

    public SharedPreferenceManager putBoolean(String key, Boolean value) {
        sPreferencesEditor.putBoolean(key, value);
        sPreferencesEditor.commit();
        return sInstance;
    }

    public float getFloat(String key, float defaultValue) {
        return sSharedPreferences.getFloat(key, defaultValue);
    }

    public SharedPreferenceManager putFloat(String key, float value) {
        sPreferencesEditor.putFloat(key, value);
        sPreferencesEditor.commit();
        return sInstance;
    }
}
