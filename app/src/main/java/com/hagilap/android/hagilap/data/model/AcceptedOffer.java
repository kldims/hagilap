package com.hagilap.android.hagilap.data.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kdimla on 4/16/16.
 */
public class AcceptedOffer {
    @Nullable
    @SerializedName("distance")
    private float distance;
    @SerializedName("details")
    private Inquiry details;
    @SerializedName("profile")
    private Profile profile;
    @SerializedName("success")
    private String success;

    public Inquiry getDetails() {
        return details;
    }

    public void setDetails(Inquiry details) {
        this.details = details;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    @Nullable
    public float getDistance() {
        return distance;
    }

    public void setDistance(@Nullable float distance) {
        this.distance = distance;
    }
}
