package com.hagilap.android.hagilap.data.api.response;

import com.google.gson.annotations.SerializedName;
import com.hagilap.android.hagilap.data.model.Profile;

/**
 * Created by kdimla on 4/16/16.
 */
public class Verifyresponse {
    @SerializedName("profile")
    private Profile profile;
    @SerializedName("token")
    private String token;

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
