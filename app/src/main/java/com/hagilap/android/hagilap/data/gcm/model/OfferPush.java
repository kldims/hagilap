package com.hagilap.android.hagilap.data.gcm.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.hagilap.android.hagilap.data.model.Profile;

/**
 * Created by kdimla on 4/16/16.
 */
public class OfferPush extends RejectOfferPush {
    @Nullable
    @SerializedName("distance")
    private float distance;

    @Nullable
    public float getDistance() {
        return distance;
    }

    public void setDistance(@Nullable float distance) {
        this.distance = distance;
    }
}
