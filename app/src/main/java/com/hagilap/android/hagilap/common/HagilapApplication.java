package com.hagilap.android.hagilap.common;

import android.app.Application;
import android.content.Intent;
import android.graphics.Typeface;

import com.google.android.gms.maps.MapsInitializer;
import com.hagilap.android.hagilap.BuildConfig;
import com.hagilap.android.hagilap.common.font.Font;
import com.hagilap.android.hagilap.common.utils.GoogleApiManager;
import com.hagilap.android.hagilap.common.utils.SharedPreferenceManager;
import com.hagilap.android.hagilap.data.service.DataService;

import timber.log.Timber;

/**
 * Created by kdimla on 4/16/16.
 */
public class HagilapApplication extends Application {
    private static GoogleApiManager mGoogleApiManager;
    private static SharedPreferenceManager mSharedPreferenceManager;

    @Override
    public void onCreate() {
        super.onCreate();
        initialize();
    }

    public static GoogleApiManager getmGoogleApiManager() {
        return mGoogleApiManager;
    }

    public static SharedPreferenceManager getmSharedPreferenceManager() {
        return mSharedPreferenceManager;
    }


    private void initialize() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        initializeManagers();
        initializeServices();
    }

    private void initializeManagers() {

        MapsInitializer.initialize(this);
        mGoogleApiManager = GoogleApiManager.getInstance(this);
        mSharedPreferenceManager = SharedPreferenceManager.getInstance(this);

        Font.AVENIR_TYPEFACE = Typeface.createFromAsset(getAssets(), Font.FONT_AVENIR);
    }

    private void initializeServices() {
        startService(new Intent(this, DataService.class));
    }


}
