package com.hagilap.android.hagilap.getoffers.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hagilap.android.hagilap.R;
import com.hagilap.android.hagilap.common.widget.AvenirTextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by kdimla on 4/16/16.
 */
public class OffersViewHolder {
    @Bind(R.id.hagilap_offers_item_name)
    AvenirTextView mName;
    @Bind(R.id.hagilap_offers_item_distance)
    AvenirTextView mDistance;
    @Bind(R.id.hagilap_offers_item_accept)
    AvenirTextView mAccept;

    public OffersViewHolder(View view) {
        ButterKnife.bind(this, view);
    }

    public AvenirTextView getmName() {
        return mName;
    }

    public AvenirTextView getmDistance() {
        return mDistance;
    }

    public AvenirTextView getmAccept() {
        return mAccept;
    }
}
