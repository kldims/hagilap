package com.hagilap.android.hagilap.data.api.request;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kdimla on 4/16/16.
 */
public class Headers {
    private interface Key {
        String TOKEN = "token";
    }

    public static Map<String, String> getTokenHeader(String token) {
        Map<String, String> header = new HashMap<>();
        header.put(Key.TOKEN, token);
        return header;
    }
}
